// EFECTO EN CAMPOS INPUT
(function() {
	// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
	if (!String.prototype.trim) {
		(function() {
			// Make sure we trim BOM and NBSP
			var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
			String.prototype.trim = function() {
				return this.replace(rtrim, '');
			};
		})();
	}

	[].slice.call( document.querySelectorAll( 'input.input--field, textarea.textarea--field' ) ).forEach( function( inputEl ) {
		// in case the input is already filled..
		if( inputEl.value.trim() !== '' ) {
			classie.add( inputEl.parentNode, 'input--filled' );
		}

		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );

	function onInputFocus( ev ) {
		classie.add( ev.target.parentNode, 'input--filled' );
	}

	function onInputBlur( ev ) {
		if( ev.target.value.trim() === '' ) {
			classie.remove( ev.target.parentNode, 'input--filled' );
		}
	}
})();

//AUTOSIZE PARA TEXTAREA
var txt = $('#input--message'),
hiddenDiv = $(document.createElement('div')),
content = null;

txt.addClass('txtcontent');
hiddenDiv.addClass('txtresizer txtboth');

$('.textarea').append(hiddenDiv);

txt.on('keyup', function () {

	content = $(this).val();

	content = content.replace(/\n/g, '<br>');
	hiddenDiv.html(content + '<br class="lbr">');

	$(this).css('height', (hiddenDiv.height() + 20));

});