function resizeImages() {
    var width = window.innerWidth || document.documentElement.clientWidth;
    $(".mobile-image").each(function() {
        var oldSrc = $(this).attr('src');
        if (width >= 690) {
            var newSrc = oldSrc.replace('-mobile.','-desktop.');
        } if (width < 690) {
            var newSrc = oldSrc.replace('-desktop.','-mobile.');
        }
        $(this).attr('src',newSrc);
    });
}

function cerrarMenu() {
    $('.mainmenu--container').removeClass('mainmenu--container__active');
    $('.mainmenu').fadeOut(350);
}

$(document).ready(function(){
    resizeImages()
    $('img.svg').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            var $svg = $(data).find('svg');
            if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }
        $svg = $svg.removeAttr('xmlns:a');
        $img.replaceWith($svg);
        }, 'xml');
    });

    $('.navbar--link__navbtn').click(function(){
        $('.mainmenu').fadeIn(350);
        $('.mainmenu--container').addClass('mainmenu--container__active');
    });

    $('.mainmenu--container .btn-close').click(function(){
        cerrarMenu();
    });

    $('.mainmenu').click(function(){
        cerrarMenu()
    }).children().click(function(e) {
      return false;
    });
})

$(window).resize(function(){
    resizeImages()
})