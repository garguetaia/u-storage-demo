<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'ustorage');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'usr_ustorage');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'U$%xs/sl1_1');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'db1.iainteractive.com.mx');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '.D9Ej-`^`:vJ2x2*%P@!AP[Lv82YLJvwz=N<usG]|6@:Y`z7/$T:{{V/L-KQ!Jf(');
define('SECURE_AUTH_KEY', '[K[XL q}F24YVr.;Ik^5 eK;ZZG&P3YqUa/h1(M6J~LcM)}lGjF$A=Lr7Zkw#JlF');
define('LOGGED_IN_KEY', 'KMs|HJPggwlD)_S|!~ZS{%-D)2L+gdg0t3>Wc|^;-XaSM=y5cI!=FdZj341:=y*G');
define('NONCE_KEY', 'f=.9Sl{|m?ChKX^_ku<K#NrIj!wVJ(+7-~MnV/r:BD18D]3*$:in]M<]zo4#s|(b');
define('AUTH_SALT', 'IU>xf#B?`QvwF%Qcp.$s0GHinv6V2P+*1g9/QNX$+Jr8[5PCK_/xvOeqC.#U?ZVu');
define('SECURE_AUTH_SALT', '#JOwmk%jZNGp8_!Qf]=(4;/).-r+$pSl(>?*@Q-9U,X_%}It .$[L4o?|y9l+xoB');
define('LOGGED_IN_SALT', '098EK_fk}}9#=BEKA+K ^)%,ho.FTl,yz7yAh-4I!Y;2:_A 2QhP!wj`w9|W?qD#');
define('NONCE_SALT', 'osA~1*E;1ARZF:5*doW|p?T8K~vHIsgD3)fz-1Mlc)K=T_>vfkHTJ,HDSky9&XDB');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

define('WPCF7_AUTOP', false );

define('WP_POST_REVISIONS', 2);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

