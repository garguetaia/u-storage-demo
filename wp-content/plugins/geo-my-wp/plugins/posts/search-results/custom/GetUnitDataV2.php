<?php 
//JSON GetUnitDataV2

//$site_id_search = 1000000149;
$id = $_GET['id'];
$site_id_search = $id;
$disccount = 0.10;

$client = new SoapClient('https://slc.centershift.com/sandbox40/SWS.asmx?WSDL',
 array('features'=>SOAP_SINGLE_ELEMENT_ARRAYS, 
   'soap_version' => SOAP_1_1,
   'encoding'=>'UTF-8',
   'trace' => 1 ));

$params = array(
 'LookupUser_Request' => array(
    'Username'=>'ST1UTest',
    'Password'=>'IcyC0ld!',
    'Channel'=> '2'  
    ),
 'Request' => array(
    'SiteID' => $site_id_search,
    'Status' => array(
      'StatusItem' => 'Available'
      ),
    ),
 ); 

$method = $client->GetSiteUnitDataV2($params);
$pre_result = json_encode($method, true);
$result = json_decode($pre_result, true);

foreach ($result as $GetSiteUnitDataV2Result){ 
  foreach ($GetSiteUnitDataV2Result as $Units){ 
    foreach ($Units as $item){ 

      $items = count($item);

      for($i=0; $i<$items; $i++){
          $unit['unit'][$i]= array(
            'unit_id' => $item[$i]['UnitId'], 
            'site_id' => $item[$i]['SiteId'],
            'street_rate' => number_format($item[$i]['StreetRate'], 2),
            'web_price' => number_format(($item[$i]['StreetRate'] - ($item[$i]['StreetRate'] * $disccount)), 2),
            'width' => number_format($item[$i]['Width'],2),
            'depth' => number_format($item[$i]['Depth'],2),
            'area' => number_format(($item[$i]['Width']*$item[$i]['Depth']),2),
            'level' => $item[$i]['Attribute1'].($item[$i]['Attribute1']=='1'?'er Nivel':'do Nivel')
          );
       }

      echo $json = json_encode($unit, true);
    }
  }
}




?>