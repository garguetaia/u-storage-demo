<?php
/**
 * Custom - Results Page.
 * @version 1.0
 * @author Eyal Fitoussi
 */
?>
<!--  Main results wrapper - wraps the paginations, map and results -->
<div class="gmw-results-wrapper gmw-results-wrapper-<?php echo $gmw['ID']; ?> gmw-pt-results-wrapper">
	
	<?php do_action( 'gmw_search_results_start' , $gmw, $post ); ?>
	
	<!-- results count -->
	<div class="gmw-results-count">
		<span><?php gmw_results_message( $gmw, false ); ?></span>
	</div>
	
	<?php do_action( 'gmw_before_top_pagination' , $gmw, $post ); ?>
	
		
	 <!-- GEO my WP Map -->
    <?php 
    if ( $gmw['search_results']['display_map'] == 'results' ) {
        gmw_results_map( $gmw );
    }
    ?>
	
	<div class="clear"></div>
	
	<?php do_action( 'gmw_search_results_before_loop' , $gmw, $post ); ?>
	
	<!--  Results wrapper -->
	<div class="gmw-posts-wrapper">
		
		<!--  this is where wp_query loop begins -->
		<?php while ( $gmw_query->have_posts() ) : $gmw_query->the_post(); ?>
			
			<!--  single results wrapper  -->
			<?php $featured = ( !empty( $post->feature ) ) ? 'gmw-featured-post' : ''; ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class( 'wppl-single-result '.$featured ); ?>>
				
				<?php do_action( 'gmw_search_results_loop_item_start' , $gmw, $post ); ?>
				
				<!-- Title -->
				<h2>
					<?php the_title(); ?>
					<?php if ( isset( $gmw['your_lat'] ) && !empty( $gmw['your_lat'] ) ) { ?><span class="radius-dis">(<?php gmw_distance_to_location( $post, $gmw ); ?>)</span><?php } ?>
				</h2>
				
				<?php do_action( 'gmw_posts_loop_after_title' , $gmw, $post ); ?>
				
				<?php if ( isset( $gmw['search_results']['featured_image']['use'] ) && has_post_thumbnail() ) { ?>
					<div class="post-thumbnail">
						<?php the_post_thumbnail( array( $gmw['search_results']['featured_image']['width'], $gmw['search_results']['featured_image']['height'] ) ); ?>
					</div>
				<?php } ?>
			
				
				
				<!--  taxonomies -->
				<div>
					<?php gmw_pt_taxonomies( $gmw, $post ); ?>
				</div>
				 

				<div class="gmw-box-gallery clearfix">
					<?php the_field('galeria_de_sucursal'); ?>

					<ul class="tab">
						<li><a href="#data" title="">DATOS</a></li>
						<li><a href="#description" title="">DESCRIPCIÓN</a></li>
						<li><a href="#bodegas" title="">BODEGAS</a></li>
					</ul>
			    	<div class="wppl-info box-tab" id="data">
		    			<!--  Address -->
		    			<div class="wppl-address">
		    				<h4>Dirección</h4>
		    				<p><?php echo $post->address; ?></p>
		    			</div>

		    			<!--  Addiotional info -->
		    		
		    			<?php if ( !empty( $gmw['search_results']['opening_hours'] ) ) { ?>
	    
					    	<?php do_action( 'gmw_search_results_before_opening_hours', $post, $gmw ); ?>
						   	
					    	<div class="opening-hours">
					    		<?php gmw_pt_days_hours( $post, $gmw ); ?>
					    	</div>
					    <?php } ?>
						    
		    			<div>	
		    				<?php gmw_additional_info( $post, $gmw, $gmw['search_results']['additional_info'], $gmw['labels']['search_results']['contact_info'], 'div' ); ?> 
		    			</div>
		    		
		    			<!--  Driving Distance -->
						<?php if ( isset( $gmw['search_results']['by_driving'] ) ) { ?>
		    				<?php gmw_driving_distance( $post, $gmw, false ); ?>
		    			<?php } ?>
		    			
		    			<?php if( get_field('promocion_sucursal') ): ?>
			    			<div class="wppl-promo">
			    				<h4>Promociones</h4>
			    				<p><?php the_field('promocion_sucursal'); ?></p>
			    			</div>
		    			<?php endif; ?>
		    			<!-- Get directions -->	 	
						<?php if ( isset( $gmw['search_results']['get_directions'] ) ) { ?>
							<div class="get-directions-link btn-link">
		    					<?php gmw_directions_link( $post, $gmw, $gmw['labels']['search_results']['directions'] ); ?>
		    				</div>
		    			<?php } ?>
			    	</div> <!-- info -->
				</div>

		    	<div class="gmw-box-description">
			    	<!--  Excerpt -->
					<?php if ( isset( $gmw['search_results']['excerpt']['use'] ) ) { ?>
						<div class="excerpt box-tab" id="description">
							<h3>DESCRIPCIÓN</h3>
							<?php gmw_excerpt( $post, $gmw, $post->post_content, $gmw['search_results']['excerpt']['count'], $gmw['search_results']['excerpt']['more'] ); ?>
						</div>
					<?php } ?>
					
					<?php do_action( 'gmw_posts_loop_after_content' , $gmw, $post ); ?>

			    	<div class="clear"></div>
			    	<?php
			    	   /* the_field('id_de_sucursal');*/
	                	$selectID = get_the_ID();
	                	$hold_array= array();
	                	$hold_posts = get_posts( array( 'post_type' => 'holds', 'posts_per_page' => -1, 'suppress_filters' => 0, 'meta_query' => array(
							array(
								'key' => 'hold_meta_box_branch',
								'value' => $selectID,
							)
						) ) );
					?>

	                <div class="bodegas box-tab" id="bodegas">
	                	<ul>
	                	<?php /*if(!empty($hold_posts)){
							foreach( $hold_posts as $hold_post ){
								echo $hold_post->post_title;
						} }*/?>
						</ul>
	                </div>
	                <?php do_action( 'gmw_posts_loop_post_end' , $gmw, $post ); ?>


	                <h3>BODEGAS DISPONIBLES</h3>

	                <table id="example" class="display" cellspacing="0" width="100%">
				        <thead>
				            <tr>
				                <th>Tamaño</th>
				                <th>Zona</th>
				                <th>Costo Regular</th>
				                <th>Costo Internet</th>
				                <th></th>
				            </tr>
				        </thead>
				        <tfoot>
				            <tr>
				                <th>Tamaño</th>
				                <th>Zona</th>
				                <th>Costo Regular</th>
				                <th>Costo Internet</th>
				                <th></th>
				            </tr>
				        </tfoot>
				        <tbody class="body-table">
				        	<script id="sites-template" type="text/x-handlebars-template">
									{{#unit}}
										<tr>
							                <td class="tamano">{{this.width}}m x {{this.depth}}m ({{this.area}}m<sup>2</sup>)</td>
							                <td>{{this.level}}</td>
							                <td>${{this.street_rate}}MXN</td>
							                <td class="price_web">${{this.web_price}}MXN</td>
							                <td>
							                	<div class="btn_reservar"
							                		data-siteid="{{this.site_id}}"
							                		data-unitid="{{this.unit_id}}"
							                		data-sucursal="<?php the_title(); ?>"
							                		data-idsuc="<?php the_field('id_de_sucursal'); ?>"
							                		data-bodega="{{this.width}}m x {{this.depth}}m ({{this.area}}m) {{this.level}}"
							                		data-costo="${{this.web_price}}MXN"
							                		>RESERVAR</div>
							                </td>
							            </tr>
									{{/unit}}
							</script>
				        </tbody>
				    </table>
	            </div>

				<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>

				<script type="text/javascript">

				jQuery(document).ready(function($) {
					jQuery('#gmw-map-wrapper-1').on('click', '.gmnoprint, .gm-style', function(){
						var id = <?php the_field('id_de_sucursal'); ?>;
						var source = jQuery("#sites-template").html();
						var template = Handlebars.compile(source);
						jQuery.getJSON('http://demo.ia.com.mx/u-storage/wp-content/plugins/geo-my-wp/plugins/posts/search-results/custom/GetUnitDataV2.php?id='+id, function( json ) {
							jQuery('.body-table').append(template(json));

							jQuery('#example').DataTable({
								"dom": '<"top"i>rt<"bottom"flp><"clear">',
								"search": false,
								"lengthMenu": [[50, 100, 200, -1], [50, 100, 200, "All"]]
							});

							jQuery( ".btn_reservar" ).on( "click", function() {
								var siteid = jQuery(this).attr('data-siteid');
								var unitid = jQuery(this).attr('data-unitid');
								var sucursal = jQuery(this).attr('data-sucursal');
								var idsuc = jQuery(this).attr('data-idsuc');
								var bodega = jQuery(this).attr('data-bodega');
								var costo = jQuery(this).attr('data-costo');
								localStorage.setItem("SiteId", siteid);
								localStorage.setItem("InutId", unitid);
								localStorage.setItem("Sucursal", sucursal);
								localStorage.setItem("IdSucursal", idsuc);
								localStorage.setItem("Bodega", bodega);
								localStorage.setItem("Costo", costo);
								window.location = 'http://demo.ia.com.mx/ustorage/simulador-form';
							});
						});
					});

					
				});
					
				</script>	    	
		    	
		    	
		    </div> <!--  single- wrapper ends -->
		    
		    <div class="clear"></div>     
	
		<?php endwhile; ?>
		<!--  end of the loop -->



	
	</div> <!--  results wrapper -->    
	
	<?php do_action( 'gmw_search_results_after_loop' , $gmw, $post ); ?>
	
	<!--  Pagination -->
	
	
</div> <!-- output wrapper -->
