<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<div id="content" class="site-content error404-text" role="main">

			<header class="page-header">
				<h1 class="page-title"><?php _e( '404', 'ustorage' ); ?></h1>
			</header>

			<div class="page-content">
				<p><?php _e( 'La página solicitada no fue encontrada.', 'ustorage' ); ?></p>
			</div><!-- .page-content -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer();