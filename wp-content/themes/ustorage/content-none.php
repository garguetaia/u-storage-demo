<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage Mazda_Ravisa
 * @since Mazda Ravisa 1.0
 */
?>

<header class="page-header">
	<h1 class="page-title"><?php _e( 'No encontrados por el momento.', 'ustorage' ); ?></h1>
</header>

<div class="page-content">
	<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'ustorage' ), admin_url( 'post-new.php' ) ); ?></p>

	<?php elseif ( is_search() ) : ?>

	<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'ustorage' ); ?></p>
	<?php get_search_form(); ?>

	<?php else : ?>

	<p><?php _e( 'Parece que no podemos encontrar lo que estás buscando. Puede que la búsqueda puede ayudar.', 'ustorage' ); ?></p>
	

	<?php endif; ?>
</div><!-- .page-content -->
