<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry__header">
		<?php
			// Page thumbnail and title.
			ustorage_page_thumbnail();
			the_title( '<h1 class="entry-title">', '</h1>' );

		?>
		<?php if( get_field('subtitle_page') ): ?>
			<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content();
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );

			edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
