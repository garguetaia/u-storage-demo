<?php
/**
 * The default template for displaying content
 *
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
if (! is_single() ) :
	$classes = array(
		'grid-item',
		$item,
	);
endif;

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<?php ustorage_page_thumbnail(); ?>

	<header class="entry-header">
		<?php

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;
		?>

		<?php if (!is_single() ) : ?>
		<div class="entry-meta">
			<?php
				if ( 'post' == get_post_type() )
					ustorage_posted_on();

				if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
			?>
			<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'ustorage' ), __( '1 Comment', 'ustorage' ), __( '% Comments', 'ustorage' ) ); ?></span>
			<?php
				endif;

				edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<?php if ( !is_single() ) : ?>
	<div class="entry-summary">
		<?php echo "<p>".excerpt(30)."</p>"; ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php if (is_single() ) : ?>
			<div class="entry-meta">
				<?php
					if ( 'post' == get_post_type() )
						printf( '<span class="entry-date">Publicado el: <time class="entry-date" datetime="%2$s">%3$s</time></span> <span class="byline">por: <span class="author vcard">%5$s</span> Categoría:</span>',
							esc_url( get_permalink() ),
							esc_attr( get_the_date( 'c' ) ),
							esc_html( get_the_date() ),
							esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
							get_the_author()
						);
						the_category( );

					if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
				?>
				<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'ustorage' ), __( '1 Comment', 'ustorage' ), __( '% Comments', 'ustorage' ) ); ?></span>
				<?php
					endif;

					edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
				?>
			</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( '<span class="meta-nav">...</span>', 'ustorage' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php endif; ?>

	<?php 
		if ( !is_single() ) :
			echo '<div class="btn-link"><a href="' . esc_url( get_permalink() ) . '" class="btn-more" rel="bookmark">LEER</a></div>';
		endif;
	?>
	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->
