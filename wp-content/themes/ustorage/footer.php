<?php
/**
 * The template for displaying the footer
 *
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?>

		</div><!-- #main -->

		<footer id="colophon" class="footer" role="contentinfo">

			<figure class="footer__logo">
				<img src="<?php bloginfo('template_url'); ?>/images/lg-ustorage-desktop.svg" alt="U-storage Renta de Mini Bodegas">
			</figure>

			<a href="<?php echo esc_url( get_page_link( 95 )); ?>" class="footer__privacy naranja">Aviso de privacidad</a>

			<ul class="socialmedia footer__socialmedia">
				<li>
					<?php do_action( 'mazdaravisa_credits' ); ?>
					<?php echo 'Ustorage ' . date("Y") . '®'; ?>
				</li>
				<?php if (get_option('my_facebook_url')) {?>
					<li>
						<a href="<?php echo get_option('my_facebook_url');?>" title="Facebook" target="_blank"><i class="genericon genericon-facebook-alt"></i></a>
					</li>
				<?php }?>
				
				<?php if (get_option('my_twitter_url')) {?>
					<li>
						<a href="<?php echo get_option('my_twitter_url');?>" title="Twitter" target="_blank"><i class="genericon genericon-twitter"></i></a>
					</li>	
				<?php }?>
				
				<?php if (get_option('my_linkedin_url')) {?>
					<li>
						<a href="<?php echo get_option('my_linkedin_url');?>" title="LinkedIn" target="_blank"><i class="genericon genericon-linkedin"></i></a>
					</li>	
				<?php }?>

				<?php if (get_option('my_youtube_url')) {?>
					<li>
						<a href="<?php echo get_option('my_youtube_url');?>" title="Youtube" target="_blank"><i class="genericon genericon-youtube"></i></a>
					</li>	
				<?php }?>
			</ul>
		</footer><!-- #colophon -->
	</div><!-- #page -->
<?php get_template_part( 'modal' ); ?>

<?php wp_footer(); ?>




<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/classie.js"></script>

<script type="text/javascript">
var templateUrl = "<?php bloginfo('template_url'); ?>";
</script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

</body>
</html>