<?php
/**
 * U-Storage functions and definitions
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see ustorage_content_width()
 *
 * @since U-Storage 1.0
 */
if ( ! isset( $content_width ) ) {
	$content_width = 474;
}

/**
 * U-Storage only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'ustorage_setup' ) ) :
/**
 * U-Storage setup.
 *
 * Set up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support post thumbnails.
 *
 * @since U-Storage 1.0
 */
function ustorage_setup() {

	/*
	 * Make U-Storage available for translation.
	 *
	 * Translations can be added to the /languages/ directory.
	 * If you're building a theme based on U-Storage, use a find and
	 * replace to change 'ustorage' to the name of your theme in all
	 * template files.
	 */
	load_theme_textdomain( 'ustorage', get_template_directory() . '/languages' );

	// This theme styles the visual editor to resemble the theme style.
	add_editor_style( array( 'css/editor-style.css', ustorage_font_url(), 'genericons/genericons.css' ) );

	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 672, 372, true );
	add_image_size( 'ustorage-full-width', 1038, 576, true );


	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary'   => __( 'Nav primary menu', 'ustorage' ),
		'secondary' => __( 'Secondary menu in top', 'ustorage' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery',
	) );

	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'ustorage_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

	// Add support for featured content.
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'ustorage_get_featured_posts',
		'max_posts' => 6,
	) );

	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // ustorage_setup
add_action( 'after_setup_theme', 'ustorage_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since U-Storage 1.0
 */
function ustorage_content_width() {
	if ( is_attachment() && wp_attachment_is_image() ) {
		$GLOBALS['content_width'] = 810;
	}
}
add_action( 'template_redirect', 'ustorage_content_width' );


/**
 * Getter function for Featured Content Plugin.
 *
 * @since U-Storage 1.0
 *
 * @return array An array of WP_Post objects.
 */
function ustorage_get_featured_posts() {
	/**
	 * Filter the featured posts to return in U-Storage.
	 *
	 * @since U-Storage 1.0
	 *
	 * @param array|bool $posts Array of featured posts, otherwise false.
	 */
	return apply_filters( 'ustorage_get_featured_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since U-Storage 1.0
 *
 * @return bool Whether there are featured posts.
 */
function ustorage_has_featured_posts() {
	return ! is_paged() && (bool) ustorage_get_featured_posts();
}

/**
 * Register three U-Storage widget areas.
 *
 * @since U-Storage 1.0
 */
function ustorage_widgets_init() {
	require get_template_directory() . '/inc/widgets.php';
	register_widget( 'U_Storage_Ephemera_Widget' );

	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'ustorage' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Menú de lado izquierdo.', 'ustorage' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Content Sidebar', 'ustorage' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Adicional barra a la derecha.', 'ustorage' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'ustorage' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Sección del footer.', 'ustorage' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'ustorage_widgets_init' );



// Register Custom Promos Type
function promos_post_type() {

	$labels = array(
		'name'                  => _x( 'Promos', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Promo', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Promociones', 'text_domain' ),
		'name_admin_bar'        => __( 'Promociones', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
		'all_items'             => __( 'Todos las promociones', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nueva promoción', 'text_domain' ),
		'add_new'               => __( 'Nueva promoción', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Editar promoción', 'text_domain' ),
		'update_item'           => __( 'Actualizar promoción', 'text_domain' ),
		'view_item'             => __( 'Ver promociones', 'text_domain' ),
		'search_items'          => __( 'Buscar promocioness', 'text_domain' ),
		'not_found'             => __( 'No promociones disponibles', 'text_domain' ),
		'not_found_in_trash'    => __( 'No products found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen principal', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen', 'text_domain' ),
		'use_featured_image'    => __( 'Utilizar como imagen principal', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Promo', 'text_domain' ),
		'description'           => __( 'Promo information pages.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor','featured-image', 'author', 'thumbnail', 'post-formats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-awards',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'promos', $args );

}
add_action( 'init', 'promos_post_type', 0 );



// Register Custom Tips Type
function tips_post_type() {

	$labels = array(
		'name'                  => _x( 'Tips', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Tip', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Tips', 'text_domain' ),
		'name_admin_bar'        => __( 'Tip', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
		'all_items'             => __( 'Todos los tips', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nuevo tip', 'text_domain' ),
		'add_new'               => __( 'Nuevo tip', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Editar tip', 'text_domain' ),
		'update_item'           => __( 'Actualizar tip', 'text_domain' ),
		'view_item'             => __( 'Ver tips', 'text_domain' ),
		'search_items'          => __( 'Buscar tips', 'text_domain' ),
		'not_found'             => __( 'No tips disponibles', 'text_domain' ),
		'not_found_in_trash'    => __( 'No products found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen principal', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen', 'text_domain' ),
		'use_featured_image'    => __( 'Utilizar como imagen principal', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Tip', 'text_domain' ),
		'description'           => __( 'Tip information pages.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor','featured-image', 'author', 'thumbnail', 'post-formats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-lightbulb',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'tip', $args );

}
add_action( 'init', 'tips_post_type', 0 );


// Register Custom Faqs Type
function faq_post_type() {

	$labels = array(
		'name'                  => _x( 'Faqs', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Faq', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Faqs', 'text_domain' ),
		'name_admin_bar'        => __( 'Faq', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
		'all_items'             => __( 'Todos las preguntas', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nueva pregunta frecuente', 'text_domain' ),
		'add_new'               => __( 'Nueva pregunta', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Editar pregunta', 'text_domain' ),
		'update_item'           => __( 'Actualizar pregunta', 'text_domain' ),
		'view_item'             => __( 'Ver pregunta frecuente', 'text_domain' ),
		'search_items'          => __( 'Buscar pregunta frecuente', 'text_domain' ),
		'not_found'             => __( 'No pregunta frecuente disponibles', 'text_domain' ),
		'not_found_in_trash'    => __( 'No products found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen principal', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen', 'text_domain' ),
		'use_featured_image'    => __( 'Utilizar como imagen principal', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Faq', 'text_domain' ),
		'description'           => __( 'Faq information pages.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor','featured-image', 'thumbnail', 'post-formats' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-format-status',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'faqs', $args );

}
add_action( 'init', 'faq_post_type', 0 );



// Register Services
function service_post_type() {

	$labels = array(
		'name'                  => _x( 'Servicios', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Servicio', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Servicios', 'text_domain' ),
		'name_admin_bar'        => __( 'Servicio', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Product:', 'text_domain' ),
		'all_items'             => __( 'Todos los servicios', 'text_domain' ),
		'add_new_item'          => __( 'Agregar nuevo servicio', 'text_domain' ),
		'add_new'               => __( 'Nuevo servicio', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Editar servicio', 'text_domain' ),
		'update_item'           => __( 'Actualizar servicio', 'text_domain' ),
		'view_item'             => __( 'Ver servicios', 'text_domain' ),
		'search_items'          => __( 'Buscar servicios', 'text_domain' ),
		'not_found'             => __( 'No servicios disponibles', 'text_domain' ),
		'not_found_in_trash'    => __( 'No products found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen principal', 'text_domain' ),
		'set_featured_image'    => __( 'Establecer imagen', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen', 'text_domain' ),
		'use_featured_image'    => __( 'Utilizar como imagen principal', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Servicio', 'text_domain' ),
		'description'           => __( 'Información de servicios.', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor','featured-image', 'thumbnail', 'comments', 'excerpt'),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'rewrite'               => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-hammer',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'servicios', $args );
	flush_rewrite_rules();

}
add_action( 'init', 'service_post_type', 0 );


add_filter ( 'wpcf7_support_html5_fallback' , '__return_true'  );


/**
 * Register Lato Google font for U-Storage.
 *
 * @since U-Storage 1.0
 *
 * @return string
 */
function ustorage_font_url() {
	$font_url = '';
	/*
	 * Translators: If there are characters in your language that are not supported
	 * by Lato, translate this to 'off'. Do not translate into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Open Sans font: on or off', 'ustorage' ) ) {
		$query_args = array(
			'family' => urlencode( 'Open Sans:700italic,300,400,800,600' ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);
		$font_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return $font_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since U-Storage 1.0
 */
function ustorage_scripts() {
	// Add Lato font, used in the main stylesheet.
	wp_enqueue_style( 'ustorage-lato', ustorage_font_url(), array(), null );

	// Add Genericons font, used in the main stylesheet.
	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.0.3' );

	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css', array(), '3.3.5' );

	//wp_enqueue_script( 'bootstrap', get_template_directory_uri() .'/js/bootstrap.min.js', array( 'jquery'   ), '', true );

	wp_enqueue_script( 'modernizr', get_template_directory_uri() .'/js/modernizr.js', array( 'jquery'   ), '', true );

	wp_enqueue_script( 'foundation', get_template_directory_uri() .'/js/foundation.min.js', array( 'jquery'   ), '', true );

	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() .'/js/imagesloaded.pkgd.min.js', array( 'jquery'   ), '', true );

	wp_enqueue_script('jquery_lazy_load', get_stylesheet_directory_uri() . '/js/lazyload.min.js', array('jquery'), '1.5.9',true);

	wp_enqueue_script('picturefill', get_stylesheet_directory_uri() . '/js/picturefill.min.js', array('jquery'), '3.0.2',true);

	wp_enqueue_script('post_ajax', get_stylesheet_directory_uri() . '/js/post.ajax.js', array('jquery'), '1.0',true);
	wp_localize_script('post_ajax', 'ajax_url',admin_url('admin-ajax.php') );

	// Load our main stylesheet.
	wp_enqueue_style( 'ustorage-style', get_stylesheet_uri() );


	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'ustorage-ie', get_template_directory_uri() . '/css/ie.css', array( 'ustorage-style' ), '20131205' );
	wp_style_add_data( 'ustorage-ie', 'conditional', 'lt IE 9' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'ustorage-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20130402' );
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		wp_enqueue_script( 'jquery-masonry' );
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		wp_enqueue_script( 'ustorage-slider', get_template_directory_uri() . '/js/jquery.bxslider.min.js', array( 'jquery' ), '20131205', true );
	}

	wp_enqueue_script( 'ustorage-imagefill', get_template_directory_uri() . '/js/jquery-imagefill.js', array( 'jquery' ), '20150315', true );
	wp_enqueue_script( 'ustorage-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150315', true );
}
add_action( 'wp_enqueue_scripts', 'ustorage_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since U-Storage 1.0
 */
function ustorage_admin_fonts() {
	wp_enqueue_style( 'ustorage-lato', ustorage_font_url(), array(), null );
}
add_action( 'admin_print_scripts-appearance_page_custom-header', 'ustorage_admin_fonts' );

if ( ! function_exists( 'ustorage_the_attached_image' ) ) :
/**
 * Print the attached image with a link to the next attached image.
 *
 * @since U-Storage 1.0
 */
function ustorage_the_attached_image() {
	$post                = get_post();
	/**
	 * Filter the default U-Storage attachment size.
	 *
	 * @since U-Storage 1.0
	 *
	 * @param array $dimensions {
	 *     An array of height and width dimensions.
	 *
	 *     @type int $height Height of the image in pixels. Default 810.
	 *     @type int $width  Width of the image in pixels. Default 810.
	 * }
	 */
	$attachment_size     = apply_filters( 'ustorage_attachment_size', array( 810, 810 ) );
	$next_attachment_url = wp_get_attachment_url();

	/*
	 * Grab the IDs of all the image attachments in a gallery so we can get the URL
	 * of the next adjacent image in a gallery, or the first image (if we're
	 * looking at the last image in a gallery), or, in a gallery of one, just the
	 * link to that image file.
	 */
	$attachment_ids = get_posts( array(
		'post_parent'    => $post->post_parent,
		'fields'         => 'ids',
		'numberposts'    => -1,
		'post_status'    => 'inherit',
		'post_type'      => 'attachment',
		'post_mime_type' => 'image',
		'order'          => 'ASC',
		'orderby'        => 'menu_order ID',
	) );

	// If there is more than 1 attachment in a gallery...
	if ( count( $attachment_ids ) > 1 ) {
		foreach ( $attachment_ids as $attachment_id ) {
			if ( $attachment_id == $post->ID ) {
				$next_id = current( $attachment_ids );
				break;
			}
		}

		// get the URL of the next image attachment...
		if ( $next_id ) {
			$next_attachment_url = get_attachment_link( $next_id );
		}

		// or get the URL of the first image attachment.
		else {
			$next_attachment_url = get_attachment_link( reset( $attachment_ids ) );
		}
	}

	printf( '<a href="%1$s" rel="attachment">%2$s</a>',
		esc_url( $next_attachment_url ),
		wp_get_attachment_image( $post->ID, $attachment_size )
	);
}
endif;


if ( ! function_exists( 'ustorage_list_authors' ) ) :
/**
 * Print a list of all site contributors who published at least one post.
 *
 * @since U-Storage 1.0
 */
function ustorage_list_authors() {
	$contributor_ids = get_users( array(
		'fields'  => 'ID',
		'orderby' => 'post_count',
		'order'   => 'DESC',
		'who'     => 'authors',
	) );

	foreach ( $contributor_ids as $contributor_id ) :
		$post_count = count_user_posts( $contributor_id );

		// Move on if user has not published a post (yet).
		if ( ! $post_count ) {
			continue;
		}
	?>

	<div class="contributor">
		<div class="contributor-info">
			<div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
			<div class="contributor-summary">
				<h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
				<p class="contributor-bio">
					<?php echo get_the_author_meta( 'description', $contributor_id ); ?>
				</p>
				<a class="button contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
					<?php printf( _n( '%d Article', '%d Articles', $post_count, 'ustorage' ), $post_count ); ?>
				</a>
			</div><!-- .contributor-summary -->
		</div><!-- .contributor-info -->
	</div><!-- .contributor -->

	<?php
	endforeach;
}
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since U-Storage 1.0
 *
 * @param array $classes A list of existing body class values.
 * @return array The filtered body class list.
 */
function ustorage_body_classes( $classes ) {
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
		$classes[] = 'masthead-fixed';
	}

	if ( is_archive() || is_search() || is_home() ) {
		$classes[] = 'list-view';
	}

	if ( ( ! is_active_sidebar( 'sidebar-2' ) )
		|| is_page_template( 'page-templates/full-width.php' )
		|| is_page_template( 'page-templates/contributors.php' )
		|| is_attachment() ) {
		$classes[] = 'full-width';
	}

	if ( is_active_sidebar( 'sidebar-3' ) ) {
		$classes[] = 'footer-widgets';
	}

	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}

	if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
		$classes[] = 'slider';
	} elseif ( is_front_page() ) {
		$classes[] = 'grid';
	}

	return $classes;
}
add_filter( 'body_class', 'ustorage_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since U-Storage 1.0
 *
 * @param array $classes A list of existing post class values.
 * @return array The filtered post class list.
 */
function ustorage_post_classes( $classes ) {
	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'ustorage_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since U-Storage 1.0
 *
 * @global int $paged WordPress archive pagination page count.
 * @global int $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function ustorage_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'ustorage' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'ustorage_wp_title', 10, 2 );


/* Add theme logo, redes sociales, Google Analytics*/
function  themeslug_theme_customizer ($wp_customize) {
	//Logo
	$wp_customize->add_section( 'themeslug_logo_section' , array(
		'title'       => __( 'Logo', 'themeslug' ),
		'priority'    => 30,
		'description' => 'Cargar un logotipo para sustituir el nombre por defecto del sitio y la descripción en la cabecera',
	) );

	$wp_customize->add_panel( 'my_custom_options', array(
		'title' => __( 'Mis Opciones', 'textdomain' ),
		'priority' => 160,
		'capability' => 'edit_theme_options',
	));

	// Section para Google Analytics
	$wp_customize->add_section( 'google_analytics_section' , array(
		'title' => __( 'Google Analytics', 'textdomain' ),
		'panel' => 'my_custom_options',
		'priority' => 1,
		'capability' => 'edit_theme_options',
	));

	// Section para Redes Sociales
	$wp_customize->add_section( 'social_section' , array(
		'title' => __( 'Redes Sociales', 'textdomain' ),
		'panel' => 'my_custom_options',
		'priority' => 2,
		'capability' => 'edit_theme_options',
	));

	//Logo
	$wp_customize->add_setting( 'themeslug_logo' );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'themeslug_logo', array(
		'label'    => __( 'Logo', 'themeslug' ),
		'section'  => 'themeslug_logo_section',
		'settings' => 'themeslug_logo',
	) ) );

	//Google Analytics
	$wp_customize->add_setting('my_google_analytics', array(
		'type' => 'theme_mod',
		'capability' => 'edit_theme_options',
	));

	$wp_customize->add_control('my_google_analytics', array(
		'label' => __( 'Código de Google Analytics', 'textdomain' ),
		'section' => 'google_analytics_section',
		'priority' => 1,
		'type' => 'textarea',
	));

	//Redes Sociales: Facebook
	$wp_customize->add_setting( 'my_facebook_url', array(
		'type' => 'option',
		'capability' => 'edit_theme_options',
	));

	$wp_customize->add_control('my_facebook_url', array(
		'label' => __( 'Facebook URL', 'textdomain' ),
		'section' => 'social_section',
		'priority' => 2,
		'type' => 'text',
	));

	//Redes Sociales: Twitter
	$wp_customize->add_setting( 'my_twitter_url', array(
		'type' => 'option',
		'capability' => 'edit_theme_options',
	));

	$wp_customize->add_control('my_twitter_url', array(
		'label' => __( 'Twitter URL', 'textdomain' ),
		'section' => 'social_section',
		'priority' => 3,
		'type' => 'text',
	));

	//Redes Sociales: Linkedin
	$wp_customize->add_setting( 'my_linkedin_url', array(
		'type' => 'option',
		'capability' => 'edit_theme_options',
	));

	$wp_customize->add_control('my_linkedin_url', array(
		'label' => __( 'LinkedIn URL', 'textdomain' ),
		'section' => 'social_section',
		'priority' => 4,
		'type' => 'text',
	));

	//Redes Sociales: Youtube
	$wp_customize->add_setting( 'my_youtube_url', array(
		'type' => 'option',
		'capability' => 'edit_theme_options',
	));

	$wp_customize->add_control('my_youtube_url', array(
		'label' => __( 'Youtube URL', 'textdomain' ),
		'section' => 'social_section',
		'priority' => 5,
		'type' => 'text',
	));
	
}
add_action( 'customize_register' , 'themeslug_theme_customizer'  );


 

function add_google_analytics_code() {
    $output = get_option('my_google_analytics');
    if ( $output <> "" ) 
        echo stripslashes($output) . "\n";
}
add_action( 'wp_footer', 'add_google_analytics_code' );

//Slider
function post_type_slider() {
    register_post_type( 'slider',
                array( 
                'label' => __('Slider', 'templatesquare'), 
                'public' => true, 
                'show_ui' => true,
                'show_in_nav_menus' => true,
                'rewrite' => true,
                'hierarchical' => false,
                'menu_position' => 5,
                'exclude_from_search' =>true,
                'menu_icon'=> 'dashicons-slides',
                'capability_type'       => 'post',
                'supports' => array(
                                     'title',
                                     'editor',
                                     'thumbnail',
                                     'excerpt')
                    ) 
                );

    register_taxonomy('category_slider', __('slider', 'templatesquare'),array('hierarchical' => true, 'label' =>  __('Slider Categoría', 'templatesquare'), 'slidercat' => __('Categoría', 'templatesquare'))
    );
}
add_action( 'init', 'post_type_slider', 0 );




//Galeria optimizar


add_filter('post_gallery', 'bxslider_picturefill_gallery', 10, 2);

function bxslider_picturefill_gallery($output, $attr) {
    global $post;
    $return = $output;

    static $instance = 0;
	$instance++;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'figure',
        'icontag' => 'div',
        'captiontag' => 'figcaption',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => '',
        'link' => '',
        'reel' => '0',
    ), $attr));


    if (($reel!="0")) return $return;

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }else if ( ! empty( $exclude ) ) {

		// Exclude attribute is present 
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );

		// Setup attachments array
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
	} else {
		// Setup attachments array
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
	}

    if (empty($attachments)) return '';

    if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
		}
		return $output;
	}

	$itemtag = tag_escape($itemtag );
	$captiontag = tag_escape( $captiontag );
	$icontag = tag_escape( $icontag );
	$valid_tags = wp_kses_allowed_html( 'post' );

    $columns = intval( $columns );
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = '';

	$size_class = sanitize_html_class( $size );
	$gallery_div = "<div id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} gallery-size-{$size_class}'>";

	$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );
    // Loop for main slider
	
	$i = 0;
    foreach ($attachments as $id => $attachment) {
		$src = wp_get_attachment_image_src( $attachment->ID);
		

		$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		if ( ! empty( $link ) && 'file' === $link ) {
			$image_output = wp_get_attachment_link2( $id, $size, false, false, false, $attr );
		} elseif ( ! empty( $link) && 'none' === $link ) {
			//$image_output = wp_get_attachment_image( $id, $size, false, $attr );
		} else {
			//$image_output = wp_get_attachment_link( $id, $size, true, false, false, $attr );
		}
		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}
		$output .= "<{$itemtag} class='gallery-item'>";
		$output .= "
			<{$icontag} class='gallery-icon {$orientation}'>
				$image_output
			</{$icontag}>";
		if ( $captiontag && trim($attachment->post_excerpt) ) {
			$output .= "
				<{$captiontag} class='wp-caption-text gallery-caption' id='$selector-$id'>
				" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";
   
	}

    // Loop for thumbnail pager nav
	
    /*foreach ($attachments as $id => $attachment) {     
        $src = wp_get_attachment_image_src( $attachment->ID, 'small-img'); 
		    $data = $i++;
        $output .= "<a data-slide-index='{$data}' href=''>\n";
        $output .= "<img src='{$src[0]}' />\n";
        $output .= "</a>\n";
   
	}*/

	$output .= '</div>';

	return $output; 

}

function wp_get_attachment_link2( $id = 0, $size = 'thumbnail', $permalink = false, $icon = false, $text = false, $attr = '' ) {
    $_post = get_post( $id );
 
    if ( empty( $_post ) || ( 'attachment' != $_post->post_type ) || ! $url = wp_get_attachment_url( $_post->ID ) )
        return __( 'Missing Attachment' );
 
    if ( $permalink )
        $url = get_attachment_link( $_post->ID );
 
    if ( $text ) {
        $link_text = $text;
    } elseif ( $size && 'none' != $size ) {
        //$link_text = wp_get_attachment_image( $_post->ID, $size, $icon, $attr );

		$alt_text = get_post_meta($_post->ID, '_wp_attachment_image_alt', true);

		$thumb_original = wp_get_attachment_image_src($_post->ID, 'full');
    	$thumb_large    = wp_get_attachment_image_src($_post->ID, 'large');
    	$thumb_medium   = wp_get_attachment_image_src($_post->ID, 'medium'); 
    	$thumb_small    = wp_get_attachment_image_src($_post->ID, 'thumbnail');

    	$thumb_original = $thumb_original[0];
		$thumb_large    = $thumb_large[0];
		$thumb_medium   = $thumb_medium[0];
		$thumb_small    = $thumb_small[0];

		$link_text = "<picture>";
		$link_text.=  "<!--[if IE 9]><video style='display: none;'><![endif]-->";
		$link_text.=  "<source data-srcset='$thumb_original' media='(min-width: 1800px)'>";
		$link_text.=  "<source data-srcset='$thumb_large' media='(min-width: 800px)'>";
		$link_text.=  "<source data-srcset='$thumb_medium' media='(min-width: 400px)'>";
		$link_text.=  "<source data-srcset='$thumb_small'>";
		$link_text.=  "<!--[if IE 9]></video><![endif]-->";
		$link_text.=  "<img class='lazyload' data-src='$thumb_small' alt='$alt_text'>";
		$link_text.=  "</picture>";

    } else {
        $link_text = '';
    }
 
    if ( trim( $link_text ) == '' )
        $link_text = $_post->post_title;

    return apply_filters( 'wp_get_attachment_link2', "<a href='$url' class='imagefill'>$link_text</a>", $id, $size, $permalink, $icon, $text );
}

/**
* Function for responsive featured images.
* Creates a  element and populates it with appropriate image sizes for different screen widths.
* Works in place of the_post_thumbnail();
*/

function simone_the_responsive_thumbnail($post_id) {
	// Check to see if there is a transient available. If there is, use it.
	if ( false === ( $thumb_data = get_transient( 'featured_image_' . $post_id ) ) ) {
	  	simone_set_image_transient($post_id);
	  	$thumb_data = get_transient( 'featured_image_' . $post_id );
	}else{
		simone_set_image_transient($post_id);
	}

	echo '<picture>';
	echo '<!--[if IE 9]><video style="display: none;"><![endif]-->';
	echo '<source srcset="'.$thumb_data['thumb_original'].'" media="(min-width: 1800px)">';
	echo '<source srcset="'.$thumb_data['thumb_large'].'" media="(min-width: 800px)">';
	echo '<source srcset="'.$thumb_data['thumb_medium'].'" media="(min-width: 400px)">';
	echo '<source srcset="'.$thumb_data['thumb_small'].'">';
	echo '<!--[if IE 9]></video><![endif]-->';
	echo '<img class="unveil" srcset="'.$thumb_data['thumb_small'].'" alt="'.$thumb_data['thumb_alt'].'">';
	echo '</picture>';
}

function simone_set_image_transient($post_id) {
  $attachment_id = get_post_thumbnail_id($post_id);
  $alt_text = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
  if ( !$alt_text ) { $alt_text = esc_html( get_the_title($post_id) ); }
    $thumb_original = wp_get_attachment_image_src($attachment_id, 'full');
    $thumb_large   = wp_get_attachment_image_src($attachment_id, 'large');
    $thumb_medium   = wp_get_attachment_image_src($attachment_id, 'medium'); 
    $thumb_small   = wp_get_attachment_image_src($attachment_id, 'thumbnail');
    $thumb_data = array(
      'thumb_original' => $thumb_original[0],
      'thumb_large'    => $thumb_large[0], 
      'thumb_medium'   => $thumb_medium[0],
      'thumb_small'    => $thumb_small[0],
      'thumb_alt'      => $alt_text
);

set_transient( 'featured_image_' . $post_id, $thumb_data, 52 * WEEK_IN_SECONDS );
}

/**
* Reset featured image transient when the post is updated
*/

add_action('save_post', 'simone_reset_thumb_data_transient');

function simone_reset_thumb_data_transient( $post_id ) {
  delete_transient( 'featured_image_' . $post_id );
  if ( has_post_thumbnail($post_id) ) {
    simone_set_image_transient($post_id);
  }
}


class Description_Walker extends Walker_Nav_Menu
{
    /**
     * Start the element output.
     *
     * @param  string $output Passed by reference. Used to append additional content.
     * @param  object $item   Menu item data object.
     * @param  int $depth     Depth of menu item. May be used for padding.
     * @param  array|object $args    Additional strings. Actually always an
     */
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
	    parent::start_el($output, $item, $depth, $args);
	    if( ! empty ( $item->description ) ){
	    	$output .= sprintf('<div class="nav_info">');
	    	$output .= sprintf('<span class="menu-image-title">%s</span>', esc_html($item->title));
	    	$output .= sprintf('<span class="menu-price">%s</span>', esc_html($item->description));
	    	$output .= sprintf('<a href="'. esc_attr( $item->url) .'">Más información</a>');
	    	$output .= sprintf('</div>');
	    }
	}
}

function ses_add_plugin_list_to_contact_form ( $tag, $unused ) {  
  
    if ( $tag['name'] != 'plugin-list' )  
        return $tag;  
  
    $args = array ( 'post_type' => 'branchs',  
                    'numberposts' => 50,  
                    'orderby' => 'title',  
                    'order' => 'ASC' );  
    $plugins = get_posts($args);  
  
    if ( ! $plugins )  
        return $tag;  
  
    foreach ( $plugins as $plugin ) {  
        $tag['raw_values'][] = $plugin->post_title;  
        $tag['values'][] = $plugin->post_title;  
        $tag['labels'][] = $plugin->post_title;
    }  
  
    return $tag;  
}  
add_filter( 'wpcf7_form_tag', 'ses_add_plugin_list_to_contact_form', 10, 2); 


/**
* Formatting textarea selects
*/

require_once ( get_template_directory() . '/inc/state-post-type.php' );
require_once ( get_template_directory() . '/inc/branch-post-type.php' );
require_once ( get_template_directory() . '/inc/bodega-post-type.php' );

function load_branch_scripts(){
	if (is_admin()) {

		// Defining scripts directory url
		$java_script_url = get_stylesheet_directory_uri().'/js/';

		// Custom Script
		wp_register_script('jquery.validate.min',$java_script_url.'jquery.validate.min.js', array('jquery'));
		wp_register_script('custom',$java_script_url.'custom.js', array('jquery'), '1.0', true);

		// Enqueue Scripts that are needed on all the pages
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery.validate.min');
		wp_enqueue_script('custom');
	}
}
add_action('admin_enqueue_scripts', 'load_branch_scripts');

add_action( 'wp_ajax_get_branch_of_state', 'get_branch_of_state' );
add_action( 'wp_ajax_nopriv_get_branch_of_state', 'get_branch_of_state' );

add_action( 'wp_ajax_get_hold_of_branch', 'get_hold_of_branch' );
add_action( 'wp_ajax_nopriv_get_hold_of_branch', 'get_hold_of_branch' );

function get_branch_of_state()
{
	global $wpdb; 
	$cid = intval( $_POST['CID'] );
	$branch_posts = get_posts( array( 'post_type' => 'branchs', 'posts_per_page' => -1, 'suppress_filters' => 0, 'meta_query' => array(
		array(
			'key' => 'branch_meta_box_state',
			'value' => $cid,
		)
	) ) );
	$branch_ops = '<option value="">'.__('Selecciona sucursal')."</option>";
	if(!empty($branch_posts)){
		foreach( $branch_posts as $branch_post ){
			$branch_ops .= '<option value="'.$branch_post->ID.'">'.$branch_post->post_title."</option>";
		}
	}
	echo $branch_ops;
	die(); // this is required to terminate immediately and return a proper response
}

function get_hold_of_branch()
{
	global $wpdb; 
	$cid = intval( $_POST['CID'] );
	$hold_posts = get_posts( array( 'post_type' => 'holds', 'posts_per_page' => -1, 'suppress_filters' => 0, 'meta_query' => array(
		array(
			'key' => 'hold_meta_box_branch',
			'value' => $cid,
		)
	) ) );
	$hold_ops = '<option value="">'.__('Selecciona una bodega')."</option>";
	if(!empty($hold_posts)){
		foreach( $hold_posts as $hold_post ){
			$hold_ops .= '<option value="'.$hold_post->ID.'">'.$hold_post->post_title."</option>";
		}
	}
	echo "$hold_ops";
	die(); // this is required to terminate immediately and return a proper response
}

function hide_state_add_new_custom_type()
{
    global $submenu;
    // replace my_type with the name of your post type
    unset($submenu['edit.php?post_type=states'][10]);
}
add_action('admin_menu', 'hide_state_add_new_custom_type');


add_action( 'init', 'wpcd_set_cookie', 1 );
function wpcd_set_cookie() {
	if(isset( $_POST[ 'select_sucursal' ] ) && isset( $_POST[ 'select_bodega' ] )) :
		$cookie_value = sanitize_text_field( $_POST[ 'select_bodega' ] );
		$cookie_value_par = sanitize_text_field( $_POST[ 'select_sucursal' ] );
		setcookie( 'select_bodega', $cookie_value, time() + (86400 * 999), "/" ); // 86400 = 1 day
		setcookie( 'select_sucursal', $cookie_value_par, time() + (86400 * 999), "/" ); // 86400 = 1 day

		header("Refresh:0");

		exit;
	endif;
}

// Modal Post 

add_action( 'wp_ajax_load-content', 'load_ajax_content');
add_action( 'wp_ajax_nopriv_load-content', 'load_ajax_content');
function paging_link_nav( $post_id ) {
	global $post;

	$post = get_post( $post_id );

	return '<nav><ul><li class="next"><a href="'. next_post_link_plus( array('loop' => 'true', 'return' => 'href') ) .'" class="button radius next-post secondary" data-id="'. next_post_link_plus( array('loop' => 'true', 'return' => 'id') ) .'">Next</a></li><li class="prev"><a href="'. previous_post_link_plus( array('loop' => 'true', 'return' => 'href') ) .'" class="button radius previous-post secondary" data-id="'. previous_post_link_plus( array('loop' => 'true', 'return' => 'id') ) .'">Previous</a></li>
</ul></nav>';
}

function load_ajax_content ( $post_id ) {
	global $wp_query, $post;

	$postID = $_POST['post_id'];

    $args = array( 'post_type' => 'promos', 'page_id' => $postID);
    $post_query = new WP_Query( $args );
    while( $post_query->have_posts() ) : $post_query->the_post(); 
		include ( get_template_directory() . '/promociones.php' );
    endwhile;           

	

	die();
}

/*function ustorage_script_infinite_scrolling(){
    wp_register_script(
        'infinite_scrolling',//name of script
        get_template_directory_uri().'/js/jquery.infinitescroll.min.js',//where the file is
        array('jquery'),//this script requires a jquery script
        null,//don't have a script version number
        true//script will de placed on footer
    );

    if(is_page( 'blog' )){ //only when we have more than 1 post
        //we'll load this script
        wp_enqueue_script('infinite_scrolling');        
    }
}

add_action('wp_enqueue_scripts', 'ustorage_script_infinite_scrolling');

function set_infinite_scrolling(){

    if(is_page( 'blog' ) ){//again, only when we have more than 1 post
    //add js script below
    ?>    
        <script type="text/javascript">
            var inf_scrolling = {   
                loading:{
                    img: "<? echo get_template_directory_uri(); ?>/images/ajax-loading.gif",
                    msgText: "Loading next posts....",
                    finishedMsg: "Posts loaded!!",
                },

                "nextSelector":".load-more .nav-previous a",

                //navSelector is a css id of page navigation
                "navSelector":".load-more",

                //itemSelector is the div where post is displayed
                "itemSelector":"article",

                //contentSelector is the div where page content (posts) is displayed
                "contentSelector":"#content"
            };
            jQuery(inf_scrolling.contentSelector).infinitescroll(inf_scrolling);
        </script>        
    <?
    }
}
add_action( 'wp_footer', 'set_infinite_scrolling',100 );*/


//Implementación de logo en administrador de Login
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/lg-ustorage-desktop.svg);
            background-size: 100%;
            width: 120px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'U-Storage';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// Add Blog Ajax.
require get_template_directory() . '/inc/ajax-filter-posts.php';

// Implement Custom Header features.
require get_template_directory() . '/inc/custom-header.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Add Customizer functionality.
require get_template_directory() . '/inc/customizer.php';

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
	require get_template_directory() . '/inc/featured-content.php';
}


function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}
