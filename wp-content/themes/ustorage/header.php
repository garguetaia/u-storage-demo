<?php
/**
 * The Header for our theme
 *
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->

<!--[if IE 9]>
<html class="ie ie9" <?php language_attributes(); ?>>
<![endif]-->


<!--[if (gt IE 9)|!(IE)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="author" content="IA Interactive" />
	<meta name="contact" content="hello@ia.com.mx" />
	<meta name="copyright" content="Copyright (c)2016 U-Storage" />
	<meta name="description" content="Renta bodega desde un mes, sin plazo forzoso. Limpias, seguras, practicas. Bodegas con oficina, seguridad, y luz, la mejor solución a problemas de espacio.">
	<meta name="keywords" content="Renta de Bodegas,Renta de MiniBodegas,Bodegas,Bodegas en Mexico,Bodegas en la Ciudad de Mexico,Renta de Mini Bodegas,Renta de Almacenes,Mini Bodegas,Minibodegas,U-Storage,Ustorage,u storage,Renta de Bodegas en el DF,Bodegas con Seguridad,Bodegas Limpias,Bodegas con Oficinas,Bodegas Seguras,Bodegas Comodas,Bodegas para muebles,Bodegas desde un mes de renta,Self Storage Mexico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="viewport" content="initial-scale=1.0, width=device-width, maximum-scale=1.0" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<!-- flush comienzo --> 
<?php flush(); ?> 
<!-- flush fin -->

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<?php if ( get_header_image() ) : ?>
	<div id="site-header">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
			<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
		</a>
	</div>
	<?php endif; ?>

	<section class="mainmenu">
		<div class="mainmenu__container">
			<img src="<?php bloginfo('template_url'); ?>/images/lg-ustorage-desktop.svg" alt="U-storage Renta de Mini Bodegas" class="lg-menu">
			<button class="btn-close">
				<img src="<?php bloginfo('template_url'); ?>/images/ico-close.svg" alt="Cerrar" class="svg">
				<span>CERRAR</span>
			</button>
			<nav class="mainmenu__linklist">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu', 'menu_id' => 'primary-menu', 'walker' => $walker, ) ); ?>
				<ul class="socialmedia">
					<?php if (get_option('my_facebook_url')) {?>
						<li>
							<a href="<?php echo get_option('my_facebook_url');?>" title="Facebook" target="_blank"><i class="genericon genericon-facebook-alt"></i></a>
						</li>
					<?php }?>
					
					<?php if (get_option('my_twitter_url')) {?>
						<li>
							<a href="<?php echo get_option('my_twitter_url');?>" title="Twitter" target="_blank"><i class="genericon genericon-twitter"></i></a>
						</li>	
					<?php }?>
					
					<?php if (get_option('my_linkedin_url')) {?>
						<li>
							<a href="<?php echo get_option('my_linkedin_url');?>" title="LinkedIn" target="_blank"><i class="genericon genericon-linkedin"></i></a>
						</li>	
					<?php }?>

					<?php if (get_option('my_youtube_url')) {?>
						<li>
							<a href="<?php echo get_option('my_youtube_url');?>" title="Youtube" target="_blank"><i class="genericon genericon-youtube"></i></a>
						</li>	
					<?php }?>
				</ul>
				<a href="<?php echo esc_url( get_page_link( 174 )); ?>">Requisitos</a>
				<a href="<?php echo esc_url( get_page_link( 95 )); ?>">Aviso de privacidad</a>
			</nav>
		</div>
	</section>

	<header id="masthead" class="top">
		<div class="top__nav">
			<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>
				<div class='site-logo'>
					<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
				</div>
			<?php else : ?>
				<h1>
					<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
						<img src="<?php bloginfo('template_url'); ?>/images/lg-ustorage-mobile.svg" alt="U-storage Renta de Mini Bodegas" class="mobile-image">
						<span>U-storage Renta de Mini Bodegas</span>
					</a>
				</h1>
			<?php endif; ?>
			<a href="#" class="navbtn">
				<img src="<?php bloginfo('template_url'); ?>/images/ico-nav.svg" class="svg" alt="Contacto">
			</a>
			<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'navbar', 'link_before' => '<span>', 'link_after' => '</span>') ); ?>
		</div>
	</header><!-- #masthead -->

	<div id="main" class="site__main">
