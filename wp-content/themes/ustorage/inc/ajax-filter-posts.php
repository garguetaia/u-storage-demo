<?php
/**
 * AJAX posts filter
 *
 */
$element = 1;

function ajax_filter_promociones_scripts() {
  // Enqueue script
  if(is_page( 'promociones' ) ){
    wp_localize_script('function','afp_vars', array(
          'afp_nonce' => wp_create_nonce( 'afp_nonce' ), // Create nonce which we later will use to verify AJAX request
          'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
        )
    );
  }
}
add_action('wp_enqueue_scripts', 'ajax_filter_promociones_scripts', 100);

// Enqueue script
function ajax_filter_posts_scripts() {
  // Enqueue script
  if(is_page( 'blog' ) ){
    wp_register_script('afp_script', get_template_directory_uri() . '/js/ajax-filter-posts.js', false, null, false);
    wp_enqueue_script('afp_script');

    wp_localize_script( 'afp_script', 'afp_vars', array(
          'afp_nonce' => wp_create_nonce( 'afp_nonce' ), // Create nonce which we later will use to verify AJAX request
          'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
        )
    );
  }
}
add_action('wp_enqueue_scripts', 'ajax_filter_posts_scripts', 100);

// Script for getting posts
function ajax_filter_get_posts( $taxonomy ) {

  $query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
  $page = esc_attr( $_POST['page'] );

  $taxonomy = $_POST['taxonomy'];

  // WP Query
  $args = array(
    'category_name' => $taxonomy,
    'post_type' => 'post',
    'post_status' => 'publish',
    'paged' => $page,
    'orderby' => 'date',
    'posts_per_page' => 9,
  );

  // If taxonomy is not set, remove key from array and get all posts
  if( !$taxonomy ) {
    unset( $args['category'] );
  }

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

    /*if($element > 6):
      $element = 1;
    endif;

    set_query_var( 'item', 'grid-item--width'.$element );*/

    get_template_part( 'content', get_post_format() );

    // $element = $element + 1;

  endwhile;

  endif;

  wp_reset_postdata();
  $data = ob_get_clean();

  die();
}

add_action('wp_ajax_filter_posts', 'ajax_filter_get_posts');
add_action('wp_ajax_nopriv_filter_posts', 'ajax_filter_get_posts');



/* POST TIPS */
function ajax_filter_tips_scripts() {
  $args = array(
    'post_type' => 'tip',
    'post_status' => 'publish',
    'posts_per_page' => 6,
  );
  $query = new WP_Query( $args );
  // Enqueue script

  $max = $query->max_num_pages;

  if(is_page( 'tips' ) ){
    wp_register_script('afp_script', get_template_directory_uri() . '/js/ajax-filter-tips.js', false, null, false);
    wp_enqueue_script('afp_script');

    wp_localize_script( 'afp_script', 'afp_vars', array(
          'afp_nonce' => wp_create_nonce( 'afp_nonce' ), // Create nonce which we later will use to verify AJAX request
          'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
          'maxPages' => $max, 
        )
    );
  }
}
add_action('wp_enqueue_scripts', 'ajax_filter_tips_scripts', 100);

// Script for getting posts
function ajax_filter_get_tips( $taxonomy ) {

  $query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
  $page = esc_attr( $_POST['page'] );

  // WP Query
  $args = array(
    'post_type' => 'tip',
    'post_status' => 'publish',
    'paged' => $page,
    'orderby' => 'date',
    'posts_per_page' => 6,
  );

  // If taxonomy is not set, remove key from array and get all posts
  if( !$taxonomy ) {
    unset( $args['category'] );
  }

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
   
    get_template_part( 'content', get_post_format() );

  endwhile;

  endif;

  wp_reset_postdata();
  $data = ob_get_clean();

  die();
}

add_action('wp_ajax_filter_tips', 'ajax_filter_get_tips');
add_action('wp_ajax_nopriv_filter_tips', 'ajax_filter_get_tips');
