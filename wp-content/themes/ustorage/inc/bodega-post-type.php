<?php
/* City Custom Post Type */
if( !function_exists( 'create_hold_post_type' ) ){
    function create_hold_post_type(){

      $labels = array(
        'name' => __( 'Bodegas'),
        'singular_name' => __( 'Bodega' ),
        'add_new' => __('Agregar nueva'),
        'add_new_item' => __('Agregar nueva bodega'),
        'edit_item' => __('Editar Bodega'),
        'new_item' => __('Nueva Bodega'),
        'view_item' => __('Ver Bodega'),
        'search_items' => __('Buscar Bodega'),
        'not_found' =>  __('Ninguna bodega encontrada'),
        'not_found_in_trash' => __('No City found in Trash'),
        'parent_item_colon' => ''
      );

      $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
		'show_in_menu' => 'edit.php?post_type=states',
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 1,
        'exclude_from_search' => true,
        'supports' => array('title','thumbnail','editor'),
        'rewrite' => array( 'slug' => __('holds', 'framework') ),
		'menu_icon' => ''
      );

      register_post_type('holds',$args);
    }
}
add_action('init', 'create_hold_post_type');

function hold_admin_head(){
//Below css will add the menu icon for Roster Slider admin menu
?>
<style type="text/css">#adminmenu .menu-icon-cities div.wp-menu-image:before { content: "\f123"; }</style>
<?php
}
add_action('admin_head', 'hold_admin_head');

add_action( 'add_meta_boxes', 'hold_meta_box_add' );
function hold_meta_box_add()
{
    add_meta_box( 'hold-meta-box-id', 'Provide Related Information', 'hold_meta_box_cb', 'holds', 'side', 'high' );
}

function hold_meta_box_cb( $post )
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom( $post->ID );
	//print_r( $values['branch_meta_box_branch']);exit;
    $selectedState = isset( $values['hold_meta_box_state'] ) ?  $values['hold_meta_box_state']: '';
    $selectedBranch = isset( $values['hold_meta_box_branch'] ) ?  $values['hold_meta_box_branch']: ''; 
    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'hold_meta_box_nonce', 'meta_box_nonce' );
	
	/* Estado */
	$state_array = array( "" => __('Selecciona estado','framework') );
	$state_posts = get_posts( array( 'post_type' => 'states', 'posts_per_page' => -1, 'suppress_filters' => 0 ) );
	if(!empty($state_posts)){
		foreach( $state_posts as $state_post ){
			$state_array[$state_post->ID] =$state_post->post_title;
		}
	}
	
	/* Sucursal */
	$branch_array = array( "" => __('Selecciona sucursal','framework') );
	if($selectedState)
	{
		$branch_posts = get_posts( array( 'post_type' => 'branchs', 'posts_per_page' => -1, 'suppress_filters' => 0, 'meta_query' => array(
			array(
				'key' => 'branch_meta_box_state',
				'value' => $selectedState,
			)
		) ) );
	}
	if(!empty($branch_posts)){
		foreach( $branch_posts as $branch_post ){
			$branch_array[$branch_post->ID] =$branch_post->post_title;
		}
	}
    ?>
    <p>
        <label for="hold_meta_box_state"><strong>Estado:</strong> </label>
        </p>
       <select name="hold_meta_box_state" id="hold_meta_box_state" class="required" required title="Seleccionar Estado">
            <?php foreach($state_array as $key=>$val){?>
            <option value="<?php echo $key;?>" <?php selected( $selectedState[0], $key ); ?>><?php echo $val;?></option>
            <?php }?>
        </select>
    <p>
        <label for="hold_meta_box_branch"><strong>Bodega:</strong> </label>
        </p>
        <select name="hold_meta_box_branch" id="hold_meta_box_branch" class="required" required  title="Seleccionar Bodega">
            <?php foreach($branch_array as $key=>$val){?>
            <option value="<?php echo $key;?>" <?php selected( $selectedBranch[0], $key ); ?>><?php echo $val;?></option>
            <?php }?>
        </select>
    </p>
    <?php   
}

add_action( 'save_post', 'hold_meta_box_save' );
function hold_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'hold_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    //if( !current_user_can( 'edit_post' ) ) return;
	
	// now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchors can only have href attribute
        )
    );
    // Make sure your data is set before trying to save it
     if( isset( $_POST['hold_meta_box_state'] ) )
        update_post_meta( $post_id, 'hold_meta_box_state',  $_POST['hold_meta_box_state'] );
	if( isset( $_POST['hold_meta_box_branch'] ) )
        update_post_meta( $post_id, 'hold_meta_box_branch',  $_POST['hold_meta_box_branch'] );	
}


/* Add Custom Columns */
if( !function_exists( 'holds_edit_columns' ) ){
    function holds_edit_columns($columns)
    {

        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __( 'Título de Bodega','framework' ),
            "branch" => __( 'Sucursal','framework' ),
			"branch1" => __( 'Estado','framework' ),
			"date" => __( 'Fecha de publicación','framework' )
        );

        return $columns;
    }
}
add_filter("manage_edit-holds_columns", "holds_edit_columns");

if( !function_exists( 'holds_custom_columns' ) ){
    function holds_custom_columns($column){
        global $post;
        switch ($column)
        {
			 case 'branch':
				$ID = get_post_meta($post->ID,'hold_meta_box_branch',true);
				echo get_the_title( $ID );
				/*if(!empty($address)){
					echo $address;
				}
				else{
					_e('No Address Provided!','framework');
				}*/
				break;
            case 'branch1':
                $ID = get_post_meta($post->ID,'hold_meta_box_state',true);
                echo get_the_title( $ID );
				/*if(!empty($address)){
                    echo $address;
                }
                else{
                    _e('No Address Provided!','framework');
                }*/
                break;
        }
    }
}
add_action("manage_posts_custom_column", "holds_custom_columns");
?>