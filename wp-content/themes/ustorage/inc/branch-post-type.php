<?php
/* State Custom Post Type */
if( !function_exists( 'create_branch_post_type' ) ){
    function create_branch_post_type(){

      $labels = array(
        'name' => __( 'Sucursal'),
        'singular_name' => __( 'Sucursal' ),
        'add_new' => __('Agregar nuevo'),
        'add_new_item' => __('Agregar sucursal'),
        'edit_item' => __('Editar sucursal'),
        'new_item' => __('Nueva sucursal'),
        'view_item' => __('Ver sucursal'),
        'search_items' => __('Buscar sucursal'),
        'not_found' =>  __('Ninguna sucursal encontrada'),
        'not_found_in_trash' => __('No State found in Trash'),
        'parent_item_colon' => ''
      );

      $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
		'show_in_menu' => 'edit.php?post_type=states',
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 1,
        'exclude_from_search' => true,
        'supports' => array('title','thumbnail','editor'),
        'rewrite' => array( 'slug' => __('branchs', 'framework') ),
		'menu_icon' => ''
      );

      register_post_type('branchs',$args);
    }
}
add_action('init', 'create_branch_post_type');

function branch_admin_head(){
//Below css will add the menu icon for Roster Slider admin menu
?>
<style type="text/css">#adminmenu .menu-icon-branchs div.wp-menu-image:before { content: "\f123"; }</style>
<?php
}
add_action('admin_head', 'branch_admin_head');

add_action( 'add_meta_boxes', 'branch_meta_box_add' );
function branch_meta_box_add()
{
    add_meta_box( 'branch-meta-box-id', 'Provide Related Information', 'branch_meta_box_cb', 'branchs', 'side', 'high' );
}

function branch_meta_box_cb( $post )
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom( $post->ID );
	//print_r( $values['branch_meta_box_state']);exit;
    $selected = isset( $values['branch_meta_box_state'] ) ?  $values['branch_meta_box_state']: '';
     
    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'branch_meta_box_nonce', 'meta_box_nonce' );
	
	/* state */
	$state_array = array( "" => __('Selecciona estado','framework') );
	$state_posts = get_posts( array( 'post_type' => 'states', 'posts_per_page' => -1, 'suppress_filters' => 0 ) );
	
	if(!empty($state_posts)){
		foreach( $state_posts as $state_post ){
			$state_array[$state_post->ID] =$state_post->post_title;
		}
	}
    ?>
    <p>
        <label for="branch_meta_box_state"><strong>Estado: </strong></label></p>
        <select name="branch_meta_box_state" id="branch_meta_box_state" class="required" required title="Please Select Country">
            <?php foreach($state_array as $key=>$val){?>
            <option value="<?php echo $key;?>" <?php selected( $selected[0], $key ); ?>><?php echo $val;?></option>
            <?php }?>
        </select>
    
    <?php   
}

add_action( 'save_post', 'branch_meta_box_save' );
function branch_meta_box_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
     
    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['meta_box_nonce'] ) || !wp_verify_nonce( $_POST['meta_box_nonce'], 'branch_meta_box_nonce' ) ) return;
     
    // if our current user can't edit this post, bail
    //if( !current_user_can( 'edit_post' ) ) return;
	
	// now we can actually save the data
    $allowed = array(
        'a' => array( // on allow a tags
            'href' => array() // and those anchors can only have href attribute
        )
    );
    // Make sure your data is set before trying to save it
     if( isset( $_POST['branch_meta_box_state'] ) )
        update_post_meta( $post_id, 'branch_meta_box_state',  $_POST['branch_meta_box_state'] );
}


/* Add Custom Columns */
if( !function_exists( 'branchs_edit_columns' ) ){
    function branchs_edit_columns($columns)
    {

        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __( 'Título de sucursal','framework' ),
            "state" => __( 'Bodega','framework' ),
			 "date" => __( 'Fecha de publicación','framework' )
        );

        return $columns;
    }
}
add_filter("manage_edit-branchs_columns", "branchs_edit_columns");

if( !function_exists( 'branchs_custom_columns' ) ){
    function branchs_custom_columns($column){
        global $post;
        switch ($column)
        {
            case 'state':
                $ID = get_post_meta($post->ID,'branch_meta_box_state',true);
                echo get_the_title( $ID );
				/*if(!empty($address)){
                    echo $address;
                }
                else{
                    _e('No Address Provided!','framework');
                }*/
                break;
        }
    }
}
add_action("manage_posts_custom_column", "branchs_custom_columns");

?>