<?php
/**
 * U-Storage Customizer support
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

/**
 * Implement Customizer additions and adjustments.
 *
 * @since U-Storage 1.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
function ustorage_customize_register( $wp_customize ) {
	// Add postMessage support for site title and description.
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector' => '.site-title a',
			'container_inclusive' => false,
			'render_callback' => 'ustorage_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector' => '.site-description',
			'container_inclusive' => false,
			'render_callback' => 'ustorage_customize_partial_blogdescription',
		) );
	}

	// Rename the label to "Site Title Color" because this only affects the site title in this theme.
	$wp_customize->get_control( 'header_textcolor' )->label = __( 'Site Title Color', 'ustorage' );

	// Rename the label to "Display Site Title & Tagline" in order to make this option extra clear.
	$wp_customize->get_control( 'display_header_text' )->label = __( 'Display Site Title &amp; Tagline', 'ustorage' );

	// Add custom description to Colors and Background controls or sections.
	if ( property_exists( $wp_customize->get_control( 'background_color' ), 'description' ) ) {
		$wp_customize->get_control( 'background_color' )->description = __( 'May only be visible on wide screens.', 'ustorage' );
		$wp_customize->get_control( 'background_image' )->description = __( 'May only be visible on wide screens.', 'ustorage' );
	} else {
		$wp_customize->get_section( 'colors' )->description           = __( 'Background may only be visible on wide screens.', 'ustorage' );
		$wp_customize->get_section( 'background_image' )->description = __( 'Background may only be visible on wide screens.', 'ustorage' );
	}

	// Add the featured content section in case it's not already there.
	$wp_customize->add_section( 'featured_content', array(
		'title'           => __( 'Featured Content', 'ustorage' ),
		'description'     => sprintf( __( 'Use a <a href="%1$s">tag</a> to feature your posts. If no posts match the tag, <a href="%2$s">sticky posts</a> will be displayed instead.', 'ustorage' ),
			esc_url( add_query_arg( 'tag', _x( 'featured', 'featured content default tag slug', 'ustorage' ), admin_url( 'edit.php' ) ) ),
			admin_url( 'edit.php?show_sticky=1' )
		),
		'priority'        => 130,
		'active_callback' => 'is_front_page',
	) );

	// Add the featured content layout setting and control.
	$wp_customize->add_setting( 'featured_content_layout', array(
		'default'           => 'grid',
		'sanitize_callback' => 'ustorage_sanitize_layout',
	) );

	$wp_customize->add_control( 'featured_content_layout', array(
		'label'   => __( 'Layout', 'ustorage' ),
		'section' => 'featured_content',
		'type'    => 'select',
		'choices' => array(
			'grid'   => __( 'Grid',   'ustorage' ),
			'slider' => __( 'Slider', 'ustorage' ),
		),
	) );
}
add_action( 'customize_register', 'ustorage_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @since U-Storage 1.7
 * @see ustorage_customize_register()
 *
 * @return void
 */
function ustorage_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @since U-Storage 1.7
 * @see ustorage_customize_register()
 *
 * @return void
 */
function ustorage_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Sanitize the Featured Content layout value.
 *
 * @since U-Storage 1.0
 *
 * @param string $layout Layout type.
 * @return string Filtered layout type (grid|slider).
 */
function ustorage_sanitize_layout( $layout ) {
	if ( ! in_array( $layout, array( 'grid', 'slider' ) ) ) {
		$layout = 'grid';
	}

	return $layout;
}

/**
 * Bind JS handlers to make Customizer preview reload changes asynchronously.
 *
 * @since U-Storage 1.0
 */
function ustorage_customize_preview_js() {
	wp_enqueue_script( 'ustorage_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20131205', true );
}
add_action( 'customize_preview_init', 'ustorage_customize_preview_js' );

/**
 * Add contextual help to the Themes and Post edit screens.
 *
 * @since U-Storage 1.0
 */
function ustorage_contextual_help() {
	if ( 'admin_head-edit.php' === current_filter() && 'post' !== $GLOBALS['typenow'] ) {
		return;
	}

	get_current_screen()->add_help_tab( array(
		'id'      => 'ustorage',
		'title'   => __( 'U-Storage', 'ustorage' ),
		'content' =>
			'<ul>' .
				'<li>' . sprintf( __( 'The home page features your choice of up to 6 posts prominently displayed in a grid or slider, controlled by a <a href="%1$s">tag</a>; you can change the tag and layout in <a href="%2$s">Appearance &rarr; Customize</a>. If no posts match the tag, <a href="%3$s">sticky posts</a> will be displayed instead.', 'ustorage' ), esc_url( add_query_arg( 'tag', _x( 'featured', 'featured content default tag slug', 'ustorage' ), admin_url( 'edit.php' ) ) ), admin_url( 'customize.php' ), admin_url( 'edit.php?show_sticky=1' ) ) . '</li>' .
				'<li>' . sprintf( __( 'Enhance your site design by using <a href="%s">Featured Images</a> for posts you&rsquo;d like to stand out (also known as post thumbnails). This allows you to associate an image with your post without inserting it. U-Storage uses featured images for posts and pages&mdash;above the title&mdash;and in the Featured Content area on the home page.', 'ustorage' ), 'https://codex.wordpress.org/Post_Thumbnails#Setting_a_Post_Thumbnail' ) . '</li>' .
				'<li>' . sprintf( __( 'For an in-depth tutorial, and more tips and tricks, visit the <a href="%s">U-Storage documentation</a>.', 'ustorage' ), 'https://codex.wordpress.org/Twenty_Fourteen' ) . '</li>' .
			'</ul>',
	) );
}
add_action( 'admin_head-themes.php', 'ustorage_contextual_help' );
add_action( 'admin_head-edit.php',   'ustorage_contextual_help' );
