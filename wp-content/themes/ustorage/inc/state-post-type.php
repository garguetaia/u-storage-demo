<?php
/* Country Custom Post Type */
if( !function_exists( 'create_state_post_type' ) ){
    function create_state_post_type(){

      $labels = array(
        'name' => __( 'Estados'),
        'singular_name' => __( 'Estado' ),
		'menu_name'           => __( 'Sucursales'),
		'all_items'           => __( 'Estados'),
        'add_new' => __('Agregar nuevo'),
        'add_new_item' => __('Agregar nuevo estado'),
        'edit_item' => __('Editar Estado'),
        'new_item' => __('Nuevo Estado'),
        'view_item' => __('Ver Estado'),
        'search_items' => __('Buscar Estado'),
        'not_found' =>  __('Ningun estado encontrado'),
        'not_found_in_trash' => __('No Country found in Trash'),
        'parent_item_colon' => ''
      );

      $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
		//'show_in_menu' => 'edit.php',
        'query_var' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        //'menu_position' => 1,
        'exclude_from_search' => true,
        'supports' => array('title','thumbnail','editor'),
        'rewrite' => array( 'slug' => __('states', 'framework') ),
		'menu_icon' => 'dashicons-location'
      );

      register_post_type('states',$args);
    }
}
add_action('init', 'create_state_post_type');

/* Add Custom Columns */
if( !function_exists( 'states_edit_columns' ) ){
    function states_edit_columns($columns)
    {

        $columns = array(
            "cb" => "<input type=\"checkbox\" />",
            "title" => __( 'Título de la bodega','framework' ),
			"date" => __( 'Fecha de publicación','framework' )
        );

        return $columns;
    }
}
add_filter("manage_edit-states_columns", "states_edit_columns");
?>