<?php
/**
 * 
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div id="content" class="site-content servicios" role="main">
			<section class="site__container">
			<?php

			$element = 1;

			$args = array(
				'post_type' => 'post',
				'post_status' => 'publish',
				'orderby' => 'date',
				'posts_per_page' => 10,
			);

			$query = new WP_Query( $args );

			$tax = 'post_tag';
			$terms = get_categories( array(
				'orderby' => 'name',
				'parent'  => 0
			) );
			$count = count( $terms );

			if ( $count > 0 ): ?>
				<nav class="post-tags">
					<h3>CATEGORÍAS</h3>
					<ul>
					<?php
					foreach ( $terms as $term ) {
				 		$term_link = get_term_link( $term, $tax );
						echo '<li class="' . $term->name . '"><a href="' . $term_link . '" class="tax-filter" title="' . $term->slug . '">' . $term->name . '</a></li> ';
				 	} ?>
			 		</ul>
				</nav>
			<?php endif;
			if ( $query->have_posts() ): ?>
				<div class="tagged-posts">
					<div class="grid-sizer"></div>
					<div class="gutter-sizer"></div>
					<?php while ( $query->have_posts() ) : $query->the_post();

						if($element > 6):
							$element = 1;
						endif;

						set_query_var( 'item', 'grid-item--width'.$element );

						get_template_part( 'content', get_post_format());

						$element = $element + 1;

					endwhile; ?>
				</div>

			<?php else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );
				endif; ?>
			</section>
	</div><!-- #content -->

</div><!-- #main-content -->
<?php

get_footer();