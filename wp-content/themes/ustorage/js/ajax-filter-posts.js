jQuery(function($){

	$('.site__container').append( '<span class="load-more"></span>' );
	var button = $('.site__container .load-more');
	var page = 2;
	var loading = false;
	var scrollHandling = {
	    allow: true,
	    reallow: function() {
	        scrollHandling.allow = true;
	    },
	    delay: 400 //(milliseconds) adjust to the highest acceptable value
	};


	$('li:first-child .tax-filter').addClass("active");

	$('.tax-filter').click( function(event) {

		// Prevent defualt action - opening tag page
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}

		// Get tag slug from title attirbute
		$(".tax-filter").removeClass('active');
		$(this).addClass('active');
		page = 2;
		var selecetd_taxonomy = $(this).attr('title');

		$('.tagged-posts').fadeOut();

		data = {
			action: 'filter_posts',
			query_vars: afp_vars.afp_nonce,
			taxonomy: selecetd_taxonomy,
		};

		$.ajax({
			type: 'post',
			url: afp_vars.afp_ajax_url,
			data: data,
			success: function( data, textStatus, XMLHttpRequest ) {
				$('.tagged-posts').html( data );
				$('.tagged-posts').append('<div class="grid-sizer"></div><div class="gutter-sizer"></div>' );
				$('.tagged-posts').fadeIn();
				console.log( textStatus );
				console.log( XMLHttpRequest );


					footerSidebar = $('.tagged-posts');
					footerSidebar.imagesLoaded().progress( function() {
						var $grid = footerSidebar.masonry( 'reload' );
						$('figure.imagefill').find("img").css({ opacity: 1 });
					
						$('figure.imagefill').imagefill();
					});
					footerSidebar.masonry( 'reload' );
				
			},
			error: function( MLHttpRequest, textStatus, errorThrown ) {
				console.log( MLHttpRequest );
				console.log( textStatus );
				console.log( errorThrown );
				$('.tagged-posts').html( 'No posts found' );
				$('.tagged-posts').fadeIn();
			}
		})

	});
	var getUrlParameter = function getUrlParameter(sParam) {
	    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
	        sURLVariables = sPageURL.split('&'),
	        sParameterName,
	        i;

	    for (i = 0; i < sURLVariables.length; i++) {
	        sParameterName = sURLVariables[i].split('=');

	        if (sParameterName[0] === sParam) {
	            return sParameterName[1] === undefined ? true : sParameterName[1];
	        }
	    }
	};

	var tech = getUrlParameter('category');

	if(tech){
		$("."+tech).find("a").trigger( "click" );
	}

	$(window).scroll(function(){
		if( ! loading && scrollHandling.allow ) {
			scrollHandling.allow = false;
			setTimeout(scrollHandling.reallow, scrollHandling.delay);
			var offset = $(button).offset().top - $(window).scrollTop();
			var selecetd_taxonomy = $(".tax-filter.active").attr('title');
			if( 2000 > offset && selecetd_taxonomy != undefined && selecetd_taxonomy != "" ) {
				loading = true;
				var data = {
					action: 'filter_posts',
					nonce: afp_vars.afp_nonce,
					page: page,
					taxonomy: selecetd_taxonomy,
				};

				$.ajax({
					type: 'post',
					url: afp_vars.afp_ajax_url,
					data: data,
					success: function( data, textStatus, XMLHttpRequest ) {
						$('.tagged-posts').append( data );
						$('.site__container').append( button );
						page = page + 1;
						loading = false;
						console.log( textStatus );
						console.log( XMLHttpRequest );


							footerSidebar = $('.tagged-posts');
							footerSidebar.imagesLoaded().progress( function() {
								var $grid = footerSidebar.masonry( 'reload' );
								$('figure.imagefill').find("img").css({ opacity: 1 });
							
								$('figure.imagefill').imagefill();
							});
							footerSidebar.masonry( 'reload' );
						
					},
					error: function( MLHttpRequest, textStatus, errorThrown ) {
						console.log( MLHttpRequest );
						console.log( textStatus );
						console.log( errorThrown );
						$('.tagged-posts').html( 'No posts found' );
						$('.tagged-posts').fadeIn();
					}
				})

			}
		}
	});
});