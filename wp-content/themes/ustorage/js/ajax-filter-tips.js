jQuery(function($){

	$('.post-tips').append( '<span class="load-more"></span>' );
	var button = $('.post-tips .load-more');
	var page = 2;
	var loading = false;
	var scrollHandling = {
	    allow: true,
	    reallow: function() {
	        scrollHandling.allow = true;
	    },
	    delay: 400 //(milliseconds) adjust to the highest acceptable value
	};

	var maxpage = afp_vars.maxPages;

	$(window).scroll(function(){
		if( !loading && scrollHandling.allow) {
			scrollHandling.allow = false;
			setTimeout(scrollHandling.reallow, scrollHandling.delay);
			var offset = $(button).offset().top - $(window).scrollTop();
			if( 2000 > offset ) {
				loading = true;
				var data = {
					action: 'filter_tips',
					nonce: afp_vars.afp_nonce,
					page: page
				};
				if(page <= maxpage){

					$.ajax({
						type: 'post',
						url: afp_vars.afp_ajax_url,
						data: data,
						success: function( data, textStatus, XMLHttpRequest ) {
							$('.post-tips').append( data );
							$('.post-tips').append( button );
							page = page + 1;
							loading = false;
							console.log( textStatus );
							console.log( XMLHttpRequest );

								onYouTubePlayerAPIReady();
								footerSidebar = $('.post-tips');
								footerSidebar.imagesLoaded().progress( function() {
									
									$('figure.imagefill').find("img").css({ opacity: 1 });
								
									$('figure.imagefill').imagefill();
								});
							
						},
						error: function( MLHttpRequest, textStatus, errorThrown ) {
							console.log( MLHttpRequest );
							console.log( textStatus );
							console.log( errorThrown );
							$('.post-tips').html( 'No posts found' );
							$('.post-tips').fadeIn();
						}
					})
				}
			}
		}
	});

	function onYouTubePlayerAPIReady() { //Important to set up the the modal inside this function, not the other way around
		
		var ytVideoID

		$('.viewvideo').on('click', function(e){  
			ytvideoID = $(this).data('video');
			e.preventDefault();
		});

		function onPlayerReady(event) {
			event.target.playVideo();
		}

		var done = false;
		function onPlayerStateChange(event) {
			if (event.data == YT.PlayerState.PLAYING && !done) {
				setTimeout(stopVideo, 6000);
				done = true;
			}
		}
		function stopVideo() {
			player.stopVideo();
		}
		
	}
});