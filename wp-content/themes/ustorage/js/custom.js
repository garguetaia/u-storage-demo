jQuery(document).ready(function(){
	jQuery('#hold_meta_box_state').change(function(e) {
		var data = {
			'action': 'get_branch_of_state',
			'CID': jQuery(this).val()
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			if(jQuery("#hold_meta_box_branch").length>0)
			{
				jQuery("#hold_meta_box_branch").html(response);
			}
		});
    });
	
	jQuery('#post').validate();
});