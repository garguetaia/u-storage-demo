
/**
 * Theme functions file
 *
 * Contains handlers for navigation, accessibility, header sizing
 * footer widgets and Featured Content slider
 *
 */

( function( $ ) {
	var body    = $( 'body' ),
		_window = $( window ),
		nav, button, menu, modalVideo;

	var map = null;

	var now = new Date();

	var _opacity = 0;

	modalVideo = $('#videoModal');
	nav = $( '#primary-navigation' );
	button = nav.find( '.menu-toggle' );
	menu = nav.find( '.nav-menu' );

	var isMobile = screen.width <= 640,
		isRetina = window.devicePixelRatio > 1,
		$imgLazyload = $('.attachment-image, .wp-post-image');

	var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    var player;

	// Enable menu toggle for small screens.
	( function() {
		if ( ! nav.length || ! button.length ) {
			return;
		}

		// Hide button if menu is missing or empty.
		if ( ! menu.length || ! menu.children().length ) {
			button.hide();

			return;
		}

		button.on( 'click.ustorage', function() {
			nav.toggleClass( 'toggled-on' );
			if ( nav.hasClass( 'toggled-on' ) ) {
				$( this ).attr( 'aria-expanded', 'true' );
				menu.attr( 'aria-expanded', 'true' );
			} else {
				$( this ).attr( 'aria-expanded', 'false' );
				menu.attr( 'aria-expanded', 'false' );
			}
		} );
		
	} )();

	/*
	 * Makes "skip to content" link work correctly in IE9 and Chrome for better
	 * accessibility.
	 *
	 * @link http://www.nczonline.net/blog/2013/01/15/fixing-skip-to-content-links/
	 */
	_window.on( 'hashchange.ustorage', function() {
		var hash = location.hash.substring( 1 ), element;

		if ( ! hash ) {
			return;
		}

		element = document.getElementById( hash );

		if ( element ) {
			if ( ! /^(?:a|select|input|button|textarea)$/i.test( element.tagName ) ) {
				element.tabIndex = -1;
			}

			element.focus();

			// Repositions the window on jump-to-anchor to account for header height.
			window.scrollBy( 0, -80 );
		}
	} );


	$( function() {
	

		//$('figure.imagefill').imagefill();

		resizeImages();

		$('img.svg').each(function(){
	        var $img = $(this);
	        var imgID = $img.attr('id');
	        var imgClass = $img.attr('class');
	        var imgURL = $img.attr('src');

	        $.get(imgURL, function(data) {
	            var $svg = $(data).find('svg');
	            if(typeof imgID !== 'undefined') {
	            $svg = $svg.attr('id', imgID);
	        }
	        if(typeof imgClass !== 'undefined') {
	            $svg = $svg.attr('class', imgClass+' replaced-svg');
	        }
	        $svg = $svg.removeAttr('xmlns:a');
	        $img.replaceWith($svg);
	        }, 'xml');
	    });

	    $('.navbtn').click(function(e){
	        $('.mainmenu').fadeIn(350);
	        $('.mainmenu__container').addClass('mainmenu__container--active');
	        e.preventDefault();
	    });

	    $('.mainmenu__container .btn-close').click(function(e){
	        cerrarMenu();
	        e.preventDefault();
	    });

	    $('.mainmenu').click(function(e){
	        cerrarMenu();
	    });

	    $('#hold_meta_box_branch').change(function(e) {
			var data = {
				'action': 'get_hold_of_branch',
				'CID': $(this).val()
			};
			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			$.post(ajaxurl, data, function(response) {
				if($("#hold_meta_box_hold").length>0)
				{
					$("#hold_meta_box_hold").html(response);
				}
			});
	    });

		onYouTubePlayerAPIReady();

		$.each($($imgLazyload), function() {
			$(this).imgLoad(function(){
				$(this).css({ opacity: 1 });
				$(this).parent('.imagefill').imagefill();
				$(this).parent().parent('.imagefill').imagefill();
			});
		});

		$('input:checkbox').change(function(){
			if($(this).is(":checked")) {
				$(this).parent().addClass("active--input");
			} else {
				$(this).parent().removeClass("active--input");
			}
		});

		detectSucursal();

		$('form.wpcf7-form input[type="date"]').change(function() {
			var $dt_to = $.datepicker.formatDate('yy-mm-dd', new Date());
			var $hour = $(this).parent().parent().parent().find('select[name="hora_prueba"]');

			
    		
			if ( $(this).val() == $dt_to ) {
				$hour.find('option[value=""]').attr('selected','selected');
				$hour.find('option').each(function() {
					var optvalue = $(this).val();
					var hours = now.getHours();
					var minutes = now.getMinutes();

					var horaArr = optvalue.split(':');
					var horas = parseInt(horaArr[0]);
					var minutos = parseInt(horaArr[1]);

					var horaDecimal = horas + (minutos / 60);
					var horaDecimalNow = (hours + (minutes / 60)) + 0.5;

					if (horaDecimal <= horaDecimalNow) {
						var option = $hour.find("option[value='" + optvalue+ "']");
	    				option.attr("disabled","disabled");
					}else{
						var option = $hour.find("option[value='" + optvalue+ "']");
						option.removeAttr("disabled","disabled");
					}
				});
			} else if ( $(this).val() < $dt_to ) {
				$hour.find('option[value=""]').attr('selected','selected');
				$hour.find('option').each(function() {
					var optvalue = $(this).val();
					var option = $hour.find("option[value='" + optvalue+ "']");
	    			option.attr("disabled","disabled");
				});
			} else {
				$hour.find('option').each(function() {
					var optvalue = $(this).val();
					var option = $hour.find("option[value='" + optvalue+ "']");
	    			option.removeAttr("disabled","disabled");
				});
			}
		
			return true;
		});

		if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {
			$('.modal').on('show.bs.modal', function() {
			// Position modal absolute and bump it down to the scrollPosition
				$(this)
					.css({
					position: 'absolute',
					marginTop: $(window).scrollTop() + 'px',
					bottom: 'auto'
					});
				// Position backdrop absolute and make it span the entire page
				//
				// Also dirty, but we need to tap into the backdrop after Boostrap 
				// positions it but before transitions finish.
				//
				setTimeout( function() {
					$('.modal-backdrop').css({
						position: 'absolute', 
						top: 0, 
						left: 0,
						width: '100%',
						height: Math.max(
							document.body.scrollHeight, document.documentElement.scrollHeight,
							document.body.offsetHeight, document.documentElement.offsetHeight,
							document.body.clientHeight, document.documentElement.clientHeight
							) + 'px'
					});
				}, 0);
			});
		}

		$(".wpcf7-submit").click(function(){
			var $hour = $(this).parent().find('select[name="hora_prueba"]');
			$(".wpcf7-submit").ajaxComplete(function(){
				if($(".wpcf7-response-output").hasClass("wpcf7-validation-errors")){
					$(".wpcf7-form-control").each(function(){
						if($(this).val().length === 0){
							$(this).focus();
							return false;
						}
					});
				}else{
					$hour.find('option').each(function() {
						var optvalue = $(this).val();
						var option = $hour.find("option[value='" + optvalue+ "']");
		    			option.removeAttr("disabled","disabled");
					});
				}
			});
		});

		/* Form Input */
		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
		}

		[].slice.call( document.querySelectorAll( 'input.input--field, textarea.textarea--field, select.input__select--field' ) ).forEach( function( inputEl ) {
			// in case the input is already filled..
			if( inputEl.value.trim() !== '' ) {
				classie.add( inputEl.parentNode.parentNode, 'input--filled' );
			}

			// events:
			inputEl.addEventListener( 'focus', onInputFocus );
			inputEl.addEventListener( 'blur', onInputBlur );
		} );

		function onInputFocus( ev ) {
			classie.add( ev.target.parentNode.parentNode, 'input--filled' );
		}

		function onInputBlur( ev ) {
			if( ev.target.value.trim() === '' ) {
				classie.remove( ev.target.parentNode.parentNode, 'input--filled' );
			}
		}

		if ( $.isFunction( $.fn.masonry ) ) {
			footerSidebar = $('.tagged-posts');
			footerSidebar.imagesLoaded().progress( function() {
				var $grid = footerSidebar.masonry( {
					itemSelector: '.grid-item',
					gutter: '.gutter-sizer',
					columnWidth: '.grid-sizer',
					percentPosition: true,
					isAnimated: true,
					isResizable: true,
					isRTL: $( 'body' ).is( '.rtl' )
				} );
				$('figure.imagefill').imagefill();
			});
		}
	

		$('.acf-map').each(function(){
			// create map
			map = new_map( $(this) );
		});

		$('.cd-faq-trigger').on('click', function(event){
			event.preventDefault();
			$(this).parent().next('.cd-faq-content').slideToggle(200).end().parent('article').toggleClass('content-visible');
		});

		/*
		 * Fixed header for large screen.
		 * If the header becomes more than 48px tall, unfix the header.
		 *
		 * The callback on the scroll event is only added if there is a header
		 * image and we are not on mobile.
		 */
		

		if ( _window.width() > 895 ) {
			var mastheadHeight = $( '#masthead' ).height(),
				toolbarOffset, mastheadOffset;

			if ( mastheadHeight > 48 ) {
				body.removeClass( 'masthead-fixed' );
			}

			if ( body.is( '.header-image' ) ) {
				toolbarOffset  = body.is( '.admin-bar' ) ? $( '#wpadminbar' ).height() : 0;
				mastheadOffset = $( '#masthead' ).offset().top - toolbarOffset;

				_window.on( 'scroll.ustorage', function() {
					if ( _window.scrollTop() > mastheadOffset && mastheadHeight < 49 ) {
						body.addClass( 'masthead-fixed' );
					} else {
						body.removeClass( 'masthead-fixed' );
					}
				} );
			}
		}
		$("li.menu-item-has-children > a" ).on('click', function(event) {
			event.preventDefault();

			if ((navigator.userAgent.match(/Tablet|iPad/i)) && (_window.width() > 895) ){
				if(_opacity == 0){
					var _top= "100px";
					_opacity= 1;
				} else {
					var _top= "-999em";
					_opacity= 0;
				}
				$(this).next("ul.sub-menu").css({opacity: _opacity, top: _top});

			} else if(_window.width() < 895) {
				$(this).next().toggle();
			}
		});

		// Focus styles for menus.
		$( '.primary-navigation, .secondary-navigation' ).find( 'a' ).on( 'focus.ustorage blur.ustorage', function() {
			$( this ).parents().toggleClass( 'focus' );
		} );
	} );

	/**
	 * @summary Add or remove ARIA attributes.
	 * Uses jQuery's width() function to determine the size of the window and add
	 * the default ARIA attributes for the menu toggle if it's visible.
	 * @since Twenty Fourteen 1.4
	 */
	function onResizeARIA() {
		if ( 781 > _window.width() ) {
			button.attr( 'aria-expanded', 'false' );
			menu.attr( 'aria-expanded', 'false' );
			button.attr( 'aria-controls', 'primary-menu' );
		} else {
			button.removeAttr( 'aria-expanded' );
			menu.removeAttr( 'aria-expanded' );
			button.removeAttr( 'aria-controls' );
		}

	}


	function onYouTubePlayerAPIReady() { //Important to set up the the modal inside this function, not the other way around
		
		var ytVideoID

		$('.viewvideo').on('click', function(e){  
			ytvideoID = $(this).data('video');
			e.preventDefault();
		});	

		$(document).on('open.zf.reveal', function () {
			if((typeof ytvideoID != 'undefined') && ytvideoID != '') {
				player = new YT.Player('feature-video', { //Add the player
					//height: '315',
					width: '800',
					videoId: ''+ytvideoID+'', //ytVideoID is the variable that holds my youtube video ID
					playerVars: {
						rel            : 0,
						theme          : 'light',
						showinfo       : 0,
						showsearch     : 0,
						autoplay       : 1,
						autohide       : 1,
						modestbranding : 1
					},
					events: {
						'onReady': onPlayerReady,
						'onStateChange': onPlayerStateChange
					}
				});
			}
			
		});
		$(document).on('closed.zf.reveal', function () {
			$(modalVideo).find('.flex-video #feature-video').remove(); // Remove the video to shut it down
			$(modalVideo).find('.flex-video #feature-video iframe').remove(); // Remove the video to shut it down
			$(modalVideo).find('.flex-video').append('<div id="feature-video" />'); //Add the div again for next time
			ytvideoID = "";
		});

		function onPlayerReady(event) {
			event.target.playVideo();
		}

		var done = false;
		function onPlayerStateChange(event) {
			if (event.data == YT.PlayerState.PLAYING && !done) {
				setTimeout(stopVideo, 6000);
				done = true;
			}
		}
		function stopVideo() {
			player.stopVideo();
		}
		
	}


	function new_map( $el ) {
		var $markers = $el.find('.marker');
		
		// vars
		var args = {
			zoom		: 16,
			center		: new google.maps.LatLng(0, 0),
			mapTypeId	: google.maps.MapTypeId.ROADMAP,
			scrollwheel : false,
			draggable   : false, 
			zoomControl : false
		};
		// create map	        	
		var map = new google.maps.Map( $el[0], args);
		
		// add a markers reference
		map.markers = [];
		
		// add markers
		$markers.each(function(){
	    	add_marker( $(this), map );	
		});
		
		// center map
		center_map( map );
		
		// return
		return map;	
	}

	function add_marker( $marker, map ) {
		// var
		var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

		// create marker
		var marker = new google.maps.Marker({
			position	: latlng,
			map			: map
		});

		// add to array
		map.markers.push( marker );

		// if marker contains HTML, add it to an infoWindow
		if( $marker.html() )
		{
			// create info window
			var infowindow = new google.maps.InfoWindow({
				content		: $marker.html()
			});

			// show info window when marker is clicked
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open( map, marker );
			});
		}

	}


	function center_map( map ) {

		// vars
		var bounds = new google.maps.LatLngBounds();

		// loop through all markers and create bounds
		$.each( map.markers, function( i, marker ){
			var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
			bounds.extend( latlng );
		});

		// only 1 marker?
		if( map.markers.length == 1 )
		{
		    map.setCenter( bounds.getCenter() );
		    map.setZoom( 16 );
		}
		else
		{
			map.fitBounds( bounds );
		}

	}

	function resizeImages() {
	    var width = window.innerWidth || document.documentElement.clientWidth;
	    $(".mobile-image").each(function() {
	        var oldSrc = $(this).attr('src');
	        if (width >= 690) {
	            var newSrc = oldSrc.replace('-mobile.','-desktop.');
	        } if (width < 690) {
	            var newSrc = oldSrc.replace('-desktop.','-mobile.');
	        }
	        $(this).attr('src',newSrc);
	    });
	}

	function cerrarMenu() {
	    $('.mainmenu__container').removeClass('mainmenu__container--active');
	    $('.mainmenu').fadeOut(350);
	}



	_window
		.on( 'load.ustorage', onResizeARIA )
		.on( 'resize.ustorage', function() {
			onResizeARIA();
	} );

	_window.on('resize', function() {
		if ((navigator.userAgent.match(/Tablet|iPad/i)) && (_window.width() > 895) ){
			$("ul.sub-menu").removeAttr("style");
			$("ul.sub-menu").css({opacity: 0, top:"-999em"});
		}
		 
		var cbSettings = {
			rel: 'cboxElement',
			width: '100%',
			height: 'auto',
			maxWidth: '100%',
			maxHeight: '100%',
			title: function() {
				return $(this).find('img').attr('alt');
			}
		}
		resizeImages();

		detectSucursal();
    });

	_window.load( function() {
		var footerSidebar,
			isCustomizeSelectiveRefresh = ( 'undefined' !== typeof wp && wp.customize && wp.customize.selectiveRefresh );


		if ( $.isFunction( $.fn.masonry ) ) {
				

			if ( isCustomizeSelectiveRefresh ) {

				// Retain previous masonry-brick initial position.
				wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function( placement ) {
					var copyPosition = (
						placement.partial.extended( wp.customize.widgetsPreview.WidgetPartial ) &&
						placement.removedNodes instanceof jQuery &&
						placement.removedNodes.is( '.masonry-brick' ) &&
						placement.container instanceof jQuery
					);
					if ( copyPosition ) {
						placement.container.css( {
							position: placement.removedNodes.css( 'position' ),
							top: placement.removedNodes.css( 'top' ),
							left: placement.removedNodes.css( 'left' )
						} );
					}
				} );

				// Re-arrange footer widgets after selective refresh event.
				wp.customize.selectiveRefresh.bind( 'sidebar-updated', function( sidebarPartial ) {
					if ( 'sidebar-3' === sidebarPartial.sidebarId ) {
						footerSidebar.masonry( 'reloadItems' );
						footerSidebar.masonry( 'layout' );
					}
				} );
			}
		}

		$.each($($imgLazyload), function() {
			$(this).imgLoad(function(){
				$(this).css({ opacity: 1 });
				$(this).parent('.imagefill').imagefill();
				$(this).parent().parent('.imagefill').imagefill();
			});
		});
	

		// Initialize audio and video players in Twenty_Fourteen_Ephemera_Widget widget when selectively refreshed in Customizer.
		if ( isCustomizeSelectiveRefresh && wp.mediaelement ) {
			wp.customize.selectiveRefresh.bind( 'partial-content-rendered', function() {
				wp.mediaelement.initialize();
			} );
		}

		// Initialize Featured Content slider.
		if ( body.is( '.slider' ) ) {
			$('.slide__bxslider').bxSlider({mode: 'fade', auto: true, speed: 800, pause: 15000});
		}
	} );
} )( jQuery );

(function($){
    $.fn.imgLoad = function(callback) {
        return this.each(function() {
            if (callback) {
                if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
                    callback.apply(this);
                }
                else {
                    $(this).on('load', function(){
                        callback.apply(this);
                    });
                }
            }
        });
    };


})(jQuery);

function detectSucursal() {
	if(detectmob()){
		jQuery(".bxslider__info__form").hide();
		jQuery(".bxslider__info__tel").show();
	}else{
		jQuery(".bxslider__info__form").show();
		jQuery(".bxslider__info__tel").hide();
	}
};

function detectmob(){if(navigator.userAgent.match(/Touch/i)||navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){return true}else{return false}}






