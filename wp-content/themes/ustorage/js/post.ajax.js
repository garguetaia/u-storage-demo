
jQuery(document).foundation();

var baseUrl = document.URL,
History = window.History || {},
Modernizr = window.Modernizr || {},
isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor),
isIosSafari = /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(navigator.userAgent) || /(iPhone|iPod|iPad).*AppleWebKit/i.test(navigator.userAgent),
isIosChrome = !!navigator.userAgent.match('CriOS'),
isHistorySupported = Modernizr.history && !!window.sessionStorage && ( !(isIosSafari || isSafari) ) || isIosChrome,
page = 0;

(function($) {

	$.fn.displayPost = function() {

		event.preventDefault();

		var post_id = $(this).data("id");
		var id = "#" + post_id;
		var pageurl = $(this).attr('href');
		var ajaxurl = ajax_url;

		// Check if the reveal modal for the specific post id doesn't already exist by checking for it's length
		if($(id).length == 0 ) {
			// We'll add an ID to the new reveal modal; we'll use that same ID to check if it exists in the future.
			var modal = $('<div data-reveal data-options="multipleOpened:false;">').attr('id', post_id ).addClass('reveal medium modalreveal').appendTo('body');
			$.ajax({
	            type: 'POST',
	            url: ajaxurl,
	            data: {"action": "load-content", post_id: post_id },
	            success: function(response) {
	            	$(document).foundation();
		            modal.empty().html(response).append('<button class="close-button" data-close aria-label="Close reveal" type="button"><span aria-hidden="true">&times;</span></button>').foundation('open');
					/*$(document).on('closeme.zf.reveal', function() {
						// Trigger resize 
						$(window).trigger('resize');
                    	return false;
                    });*/

					footerSidebar = $('.modalreveal');
					footerSidebar.imagesLoaded().progress( function() {
						$('figure.imagefill').find("img").css({ opacity: 1 });
					
						$('figure.imagefill').imagefill();
					});
					bindEm(id);
					/*if (isHistorySupported) {
						history.pushState(null, null,pageurl);
						page ++;
					}*/
		 		}
			});
		}
		 //If the div with the ID already exists just open it.
	     else {
		    $(id).foundation('open');
		    /*if (isHistorySupported) {
				history.pushState(null, null,pageurl);
				page ++;
			}*/
	    }

	     // Recalculate left margin on window resize to allow for absolute centering of variable width elements
	     $(window).resize(function(){
	    	 var left;
			    left = Math.max($(window).width() - $(id).outerWidth(), 0) / 2;
			    /*$(id).css({
			        left:left + $(window).scrollLeft()
			    });*/
		 });
	}

})(jQuery);

// Apply the function when we click on the .reveal link

jQuery('.revealpost').on("click", function(e) {
	jQuery(this).displayPost();
	e.preventDefault();
});


jQuery(document).on("click", ".secondary", function(e) {
	jQuery(this).displayPost();
	e.preventDefault();
});

jQuery(document).on('closed.zf.reveal', function () {
	setTimeout(function() {
        if ($('.reveal').filter(':visible').length === 0) {
        	if (isHistorySupported && page != 0) {
				window.history.go(-page);
				page = 0;
			}
		}
    }, 200);
});


function bindEm(id) { 
	var this_ = $(id);
	var btnNext, btnBack;

	btnNext = this_.find('.next-post');
	btnBack = this_.find('.previous-post');

	jQuery(document).on("keydown", id,function(e) {
		if(e.which == 37) { // left
			jQuery(btnBack).displayPost();
		}
		else if(e.which == 39) { // right
			jQuery(btnNext).displayPost();
		}
	});
}


