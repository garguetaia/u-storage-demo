<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?>

<div id="videoModal" class="reveal large" data-reveal="" aria-hidden="false" tabindex="0" data-overlay="true" data-options="multipleOpened:false;">
	<h2 id="videoModalTitle"></h2>
	<div class="flex-video widescreen">
		<div id="feature-video">[this div will be converted to an iframe]</div>
	</div>

  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
