<?php
/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && ustorage_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


	<div id="content" class="site-content" role="main">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry__header">
					<?php
						// Page thumbnail and title.
						ustorage_page_thumbnail();
						the_title( '<h1 class="entry-title">', '</h1>' );

					?>
					<?php if( get_field('subtitle_page') ): ?>
						<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
					<?php endif; ?>

					<?php if( get_field('video_quienes_somos') ): ?>
						<div class="video-container">
							<iframe src="https://www.youtube.com/embed/<?php the_field('video_quienes_somos'); ?>" frameborder="0" allowfullscreen></iframe>
						</div>
					<?php endif; ?>	
				</header><!-- .entry-header -->

				<section class="about">
					<figure>
						<img src="<?php bloginfo('template_url'); ?>/images/lg-ustorage-desktop.svg" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					</figure>

					<div class="about__description">
						<?php
							the_content();
						?>
					</div>
				</section><!-- .entry-content -->

				<ul class="about__benefits">
					<li class="security">
						<i><img src="<?php bloginfo('template_url'); ?>/images/icon-pc.svg"></i>
						<p>Acceso computarizado automático mediante un password que se teclea desde su auto, el cual permite la entrada a personas autorizadas solamente</p>
					</li>

					<li class="confort">
						<i><img src="<?php bloginfo('template_url'); ?>/images/ico-camera.svg"></i>
						<p>Cámaras de Televisión en múltiples puntos con grabación continua. </p>
					</li>
					<li class="clean">
						<i><img src="<?php bloginfo('template_url'); ?>/images/ico-segurity.svg"></i>
						<p>Usted pone su propio candado y usted conserva la llave. Nadie toca o ve los bienes que se almacenan en cada bodega. </p>
					</li>
					<li class="access">
						<i><img src="<?php bloginfo('template_url'); ?>/images/ico-lookout.svg"></i>
						<p>Vigilancia las 24 horas los 365 días del año. </p>
					</li>
				</ul>

				<section class="about__services">
					<div class="about__services__container">
						<div class="benefits_box">
							<?php 
							$image_beneficios = get_field('imagen_beneficios');
							if( !empty($image_beneficios) ): ?>
								<figure class="benefits_box__foto">
									<img src="<?php echo $image_beneficios['url']; ?>" alt="<?php echo $image_beneficios['alt']; ?>" />
								</figure>
							<?php endif; ?>
							<div class="group_text">
								<?php if( get_field('titulo_beneficios') ): ?>
									<h4 class="subtitle naranja"><?php the_field('titulo_beneficios'); ?></h4>
								<?php endif; ?>
								<?php if( get_field('descripcion_de_beneficios') ): ?>
									<div class="txts"><?php the_field('descripcion_de_beneficios'); ?></div>
								<?php endif; ?>
							</div>
							
						</div>

						<div class="benefits_box">
							<?php 
							$image_bodegas = get_field('imagen_bodegas');
							if( !empty($image_bodegas) ): ?>
								<figure class="benefits_box__foto">
									<img src="<?php echo $image_bodegas['url']; ?>" alt="<?php echo $image_bodegas['alt']; ?>" />
								</figure>
							<?php endif; ?>
							<div class="group_text">
								<?php if( get_field('titulo_de_bodegas') ): ?>
									<h4 class="subtitle naranja"><?php the_field('titulo_de_bodegas'); ?></h4>
								<?php endif; ?>
								<?php if( get_field('descripcion_de_bodegas') ): ?>
									<div class="txts"><?php the_field('descripcion_de_bodegas'); ?></div>
								<?php endif; ?>
							</div>
							
						</div>
					</div>
				</section>
			</article><!-- #post-## -->


				<?php // If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			endwhile;
		?>
	</div><!-- #content -->

</div><!-- #main-content -->
<?php

get_footer();
