<?php
/**
 * Template Name: Contacto
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && ustorage_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


	<div id="content" class="site-content contact" role="main">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); ?>

				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry__header">
					<?php
						// Page thumbnail and title.
						ustorage_page_thumbnail();
						the_title( '<h1 class="entry-title">', '</h1>' );

					?>
					<?php if( get_field('subtitle_page') ): ?>
						<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
					<?php endif; ?>
				</header><!-- .entry-header -->

				<section class="home__contact">
					<?php
						the_content();
					?>
				</section><!-- .entry-content -->
			</article><!-- #post-## -->


				<?php // If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}
			endwhile;
		?>
	</div><!-- #content -->

</div><!-- #main-content -->
<?php

get_footer();
