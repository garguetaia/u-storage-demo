<?php
/**
 * Template name: Faqs
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
get_header(); ?>

<div id="main-content" class="main-content">
	<div id="content" class="site-content servicios" role="main">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); ?>

				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry__header">
					<?php
						// Page thumbnail and title.
						ustorage_page_thumbnail();
						the_title( '<h1 class="entry-title">', '</h1>' );

					?>
					<?php if( get_field('subtitle_page') ): ?>
						<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
					<?php endif; ?>
				</header><!-- .entry-header -->
				<?php the_content(); ?>
			</article><!-- #post-## -->


				<?php // If comments are open or we have at least one comment, load up the comment template.
			endwhile;
		?>
			
	</div><!-- #content -->

</div><!-- #main-content -->
<?php
get_sidebar("faqs");
get_footer();