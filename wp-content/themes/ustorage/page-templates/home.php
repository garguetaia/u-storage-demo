<?php
/**
 * Template Name: Home
 *
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

<div id="main-home" class="site__main--home">

	<section id="slider" class="slide">
		<!-- start Basic Jquery Slider -->
		<ul class="slide__bxslider">

			<?php query_posts(array('post_type' => 'slider','orderby' => 'menu_order'));
			if(have_posts()) : 
				while( have_posts() ) : the_post();
			?>
				<li>
					<?php if( get_field('enlace_de_slide') ): ?>
						<a href="<?php the_field('enlace_de_slide'); ?>" title="<?php the_title_attribute(); ?>">
					<?php endif; ?>
						<figure class="imagefill">
							<?php the_post_thumbnail('homeslider'); ?>
						</figure>

						<?php $cc = get_the_content(); ?>
						
						<div class="bxslider">
							<h2 class="bxslider__title"><span><?php the_title(); ?></span></h2>
							<div class="bxslider__info">
								<?php if( get_field('subtitulo_slide') ): ?>
									<h3 class="titulosec"><?php the_field('subtitulo_slide'); ?></h3>
								<?php endif; ?>
								<?php if($cc != '') : ?>
								
								<?php endif; ?>
								<?php if( get_field('formulario_de_reserva') ): ?>
								<div class="bxslider__info__form">
									<form action="<?php bloginfo('url'); ?>/sucursales/" method="get" accept-charset="utf-8">
										<div class="select">
											<?php
												$selectedBranch = isset( $_COOKIE[ 'select_sucursal' ] ) ?  $_COOKIE[ 'select_sucursal' ]: '';
												$selectedHold = isset( $_COOKIE[ 'select_bodega' ] ) ?  $_COOKIE[ 'select_bodega' ]: ''; 

												$branch_array = array( "" => __('Selecciona una sucursal','framework') );
												$branch_posts = get_posts( array( 'post_type' => 'branchs', 'posts_per_page' => -1, 'suppress_filters' => 0 ) );
												
												if(!empty($branch_posts)){
													foreach( $branch_posts as $branch_post ){
														$branch_array[$branch_post->ID] =$branch_post->post_title;
													}
												}
											?>
											<select name="gmw_address[]" id="hold_meta_box_branch" required title="Seleccionar una sucursal">
												<?php foreach($branch_array as $key=>$val){?>
													<option value="<?php echo do_shortcode('[gmw_post_info post_id="'.$key.'"]');?>" <?php selected( $selectedBranch, $key ); ?>><?php echo $val;?></option>
												<?php }?>
											</select>
											<input type="hidden" name="gmw_distance" value="300">
											<input type="hidden" name="gmw_units" value="metric">
											<input type="hidden" name="gmw_post" value="action">
											<input type="hidden" name="gmw_px" value="pt">
										</div>
										<!-- <div class="select"> -->
											<?php
												/***
												$hold_array = array( "" => __('Selecciona una bodega','framework') );
												if($selectedBranch)
												{
													
													$hold_posts = get_posts( array( 'post_type' => 'holds', 'posts_per_page' => -1, 'suppress_filters' => 0, 'meta_query' => array(
														array(
															'key' => 'hold_meta_box_branch',
															'value' => $selectedBranch,
														)
													) ) );
												}
												if(!empty($hold_posts)){
													foreach( $hold_posts as $hold_post ){
														$hold_array[$hold_post->ID] =$hold_post->post_title;
													}
												} ***/
											?>

											<!-- <select name="select_bodega" id="hold_meta_box_hold" required title="Seleccionar una bodega"> -->
												<?php /*** foreach($hold_array as $key=>$val){ ***/ ?>
													<!-- <option value="<?php echo $key;?>" <?php selected( $selectedHold, $key ); ?>><?php echo $val;?></option> -->
												<?php /*** } ***/?>
											<!-- </select> -->
										<!-- </div> -->
										<button type="submit" class="btn-send">RESERVA</button>
									</form>
								</div>
								<?php endif; ?>
								<?php if( get_field('formulario_de_reserva') ): ?>
									<div class="bxslider__info__tel">
									<?php if( get_field('telefono_slide') ): 
										$fieldname = get_field('telefono_slide');
	     								$classname = str_replace(' ', '', $fieldname);
	     							?>
	     								<h3><?php the_field('telefono_slide'); ?></h3>
										<a href="tel:<?php echo $classname; ?>" title="<?php the_title_attribute(); ?>" class="btn-send">MARCAR</a>
									<?php endif; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>
					<?php if( get_field('enlace_de_slide') ): ?>
						</a>
					<?php endif; ?>
				</li>
			<?php 
				endwhile; 
			endif; 
			wp_reset_query(); ?>
		</ul>
		<!-- end Basic jQuery Slider -->
	</section>


	<section class="home__video">
		<div>
			<?php if( get_field('title_about') ): ?>
				<h3 class="subtitle blanco"><?php the_field('title_about'); ?></h3>
			<?php endif; ?>
			<?php if( get_field('description_about') ): ?>
				<p class="txts"><?php the_field('description_about'); ?></p>
			<?php endif; ?>
		</div>
		<div>
			<?php if( get_field('video_youtube') ): ?>
			<a class="viewvideo feature-modal-btn" data-open="videoModal" data-video="<?php the_field('video_youtube'); ?>">
				<img src="<?php bloginfo('template_url'); ?>/images/ico-play.svg" alt="Play">
				<span>VE NUESTRO VIDEO</span>
			</a>
			<?php endif; ?>
		</div>
		<?php 

		$image = get_field('imagen_de_fondo_home');

		if( !empty($image) ): ?>
			<figure class="imagefill home__video__bg">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="attachment-image" />
			</figure>
		<?php endif; ?>
	</section>


	<section class="home__services">
		<?php if( get_field('titulo_servicios') ): ?>
			<h3 class="subtitle morado"><?php the_field('titulo_servicios'); ?></h3>
		<?php endif; ?>
		<div class="home__services__container">
			<?php

			$post_objects = get_field('servicios_disponibles');

			if( $post_objects ): 

				foreach( $post_objects as $post):
					setup_postdata($post);

				?>
			    
			    <div class="service">
			    	<div class="group_text">
						<h4 class="subtitle naranja"><?php the_title(); ?></h4>
						<div class="txts"><?php the_excerpt(); ?></div>
					</div>
					<?php if ( has_post_thumbnail() ) : ?>
						<figure class="service__foto">
							<?php the_post_thumbnail("large"); ?>
						</figure>
					<?php endif; ?>
					<div class="btn-link">
						<a href="<?php bloginfo('url'); ?>/servicios/" >CONOCE MÁS</a>
						<!-- data-id="<?php the_ID(); ?>" class="revealpost"-->
					</div>
				</div>
			     <?php endforeach; ?>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
		</div>
	</section>

	<section class="home__benefits">
		<h4 class="subtitle morado">BENEFICIOS</h4>
		<ul class="benefits">
			<li class="benefits--icon">
				<a href="<?php bloginfo('url'); ?>/quienes-somos/">
					<figure class="benefits--icon__security">
						<img src="<?php bloginfo('template_url'); ?>/images/ico-seguridad.svg" alt="Seguridad">
					</figure>
					<span>SEGURIDAD</span>
				</a>
			</li>
			<li class="benefits--icon">
				<a href="<?php bloginfo('url'); ?>/quienes-somos/">
					<figure class="benefits--icon__confort">
						<img src="<?php bloginfo('template_url'); ?>/images/ico-comodidad.svg" alt="Comodidad">
					</figure>
					<span>COMODIDAD</span>
				</a>
			</li>
			<li class="benefits--icon">
				<a href="<?php bloginfo('url'); ?>/quienes-somos/">
					<figure class="benefits--icon__clean">
						<img src="<?php bloginfo('template_url'); ?>/images/ico-limpieza.svg" alt="Limpieza">
					</figure>
					<span>LIMPIEZA</span>
				</a>
			</li>
			<li class="benefits--icon">
				<a href="<?php bloginfo('url'); ?>/quienes-somos/">
					<figure class="benefits--icon__access">
						<img src="<?php bloginfo('template_url'); ?>/images/ico-accesabilidad.svg" alt="Accsesibilidad">
					</figure>
					<span>ACCESIBILDIAD</span>
				</a>
			</li>
		</ul>
	</section>

	<section class="home__contact">
		
		<?php
			the_content();
		?>
		
		<div class="btn-link">
			<a href="#">CONTACTO</a>
		</div>
	</section>

<?php
	if ( is_front_page() && ustorage_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_footer();
