<?php
/**
 * The Template for displaying all single posts
 * Template Name: Modelos
 * @package WordPress
 * @subpackage Mazda_Ravisa
 * @since Mazda Ravisa 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content modelos" role="main">
			
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); 

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php if ( is_search() ) : ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php else : ?>

						<div class="cf content-modelos">
						        <?php 
						        the_content( sprintf(
									__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'mazdaravisa' ),
									the_title( '<span class="screen-reader-text">', '</span>', false )
								) );

								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mazdaravisa' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
								) );
						         ?>
						        <div class="cf">
						        	<div class="col-md-8">
						        		<?php the_field('galeria_360'); ?>
						        		<span class="ico-360"><img src="<?php echo get_bloginfo('template_directory');?>/images/ico-360.png" alt=""></span>
									</div>
									<div class="description col-md-4">
										
										<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && mazdaravisa_categorized_blog() ) : ?>
										<div class="entry-meta">
											<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'mazdaravisa' ) ); ?></span>
										</div>
										<?php
											endif;

											if ( is_single() ) :
												the_title( '<h1 class="modelo-title">', '</h1>' );
											else :
												the_title( '<h1 class="modelo-title">', '</h1>' );
											endif;
										?>
										<h2><?php the_field('subtitulo'); ?></h2>

										<div class="entry-meta">
											<?php
												if ( 'post' == get_post_type() )
													mazdaravisa_posted_on();

												if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
											?>
											<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'mazdaravisa' ), __( '1 Comment', 'mazdaravisa' ), __( '% Comments', 'mazdaravisa' ) ); ?></span>
											<?php
												endif;

												edit_post_link( __( 'Edit', 'mazdaravisa' ), '<span class="edit-link">', '</span>' );
											?>
										</div><!-- .entry-meta -->
										<?php if( get_field('precios_y_modelos') ): ?>
											<div class="precio">
												<span>Desde:</span>
												<?php the_field('precios_y_modelos'); ?>
											</div>
										<?php endif; ?>

										<?php if( get_field('opciones_de_color') ): ?>
											<div class="opciones-color">
												<p><span class="genericon genericon-play"></span>Disponible en los siguientes colores</p>
												<div class="list-colors"><?php the_field('opciones_de_color'); ?></div>	
											</div>
										<?php endif; ?>
										
									</div>
						        </div>
						        <?php if( get_field('galeria') ): ?>
							        <div id="gallerymodelo" class="box_gallery">
										<?php the_field('galeria'); ?>
									</div>
								<?php endif; ?>

						        <div class="no_te_detengas cf">
						        	<div class="col-md-6">
										<h2>No te detengas</h2>
										<?php the_field('descripcion_de_no_te_detengas'); ?>
									</div>
									<?php if( get_field('imagen_de_no_te_detengas') ): ?>
										<figure id="car_detail" class="animated">
											<figcaption>
												<?php if( get_field('rendimiento_combustible') ): ?>
												<div id="num_rendimiento" class="rendimiento animated">
													<span>RENDIMIENTO DE COMBUSTIBLE</span>
													<p><strong><?php the_field('rendimiento_combustible'); ?></strong><span> KM/L</span></p>
												</div>
												<?php endif; ?>
												<?php if( get_field('caballaje') ): ?>
												<div id="num_caballaje" class="caballaje animated">
													<span>CABALLAJE</span>
													<p><strong><?php the_field('caballaje'); ?></strong><span> HP</span></p>
													<em>@ <?php the_field('revoluciones_por_minuto'); ?> RPM</em>
												</div>
												<?php endif; ?>
											</figcaption>
											<?php
												$image = get_field('imagen_de_no_te_detengas');
												echo wp_get_attachment_image($image, 'full');
											?>

										</figure>
									<?php endif; ?>
									<ul class="dowloand-pdf">
										<?php if( get_field('archivo_ficha_tecnica') ): ?>
										<li>
											<a href="<?php the_field('archivo_ficha_tecnica'); ?>" class="button btndow" target="_blank" title="Descargar Ficha Técnica">Descargar Ficha Técnica</a>
										</li>
										<?php endif; ?>
										<?php if( get_field('archivo_catalogo_pdf') ): ?>
										<li>
											<a href="<?php the_field('archivo_catalogo_pdf'); ?>" class="button btndow" target="_blank" title="Descargar Cátalogo">Descargar Cátalogo</a>
										</li>
										<?php endif; ?>
									</ul>
						        </div>
								
								<div class="garantia cf">
									<?php if( get_field('imagen_de_garantia') ): ?>
									<figure class="imagefill">

										<?php
											$image = get_field('imagen_de_garantia');
											echo wp_get_attachment_image($image, 'image');
										?>
									</figure>
									<?php endif; ?>
									<div class="col-md-6">
										<h2>Garantía</h2>
										<p>
											<?php the_field('garantia'); ?>
										</p>
									</div>
								</div>
								<div class="precio_servicios cf">
									<div class="col-md-6">
										<h2>Precio de servicios</h2>
										<ul class="cf list_precio_servicios">
											<?php if( get_field('precio_10') ): ?>
											<li>
												<a href="#km10" title="">
													<p class="kilometros">10, 30, 50, 70 & 90<span>MIL KM</span></p>
													<p class="price">$<strong><?php echo number_format( floatval( get_field('precio_10') ), 0, '.', ',' ); ?></strong><em>MXN</em></p>
												</a>
											</li>
											<?php endif; ?>
											<?php if( get_field('precio_20') ): ?>
											<li>
												<a href="#km20" title="">
													<p class="kilometros">20 & 60<span>MIL KM</span></p>
													<p class="price">$<strong><?php echo number_format( floatval( get_field('precio_20') ), 0, '.', ',' ); ?></strong><em>MXN</em></p>
												</a>
											</li>
											<?php endif; ?>
											<?php if( get_field('precio_40') ): ?>
											<li>
												<a href="#km40" title="">
													<p class="kilometros">40 & 80<span>MIL KM</span></p>
													<p class="price">$<strong><?php echo number_format( floatval( get_field('precio_40') ), 0, '.', ',' ); ?></strong><em>MXN</em></p>
												</a>
											</li>
											<?php endif; ?>
											<?php if( get_field('precio_100') ): ?>
											<li>
												<a href="#km100" title="">
													<p class="kilometros">100<span>MIL KM</span></p>
													<p class="price">$<strong><?php echo number_format( floatval( get_field('precio_100') ), 0, '.', ',' ); ?></strong><em>MXN</em></p>
												</a>
											</li>
											<?php endif; ?>
										</ul>
										<button class="btn-addAuto" type="submit">Quiero este Mazda</button>
									</div>
									<div class="group_tab">
										<div id="km10" class="tab">
											<?php if( get_field('precio_10') ): ?>
												<p class="price"><strong>$ <?php the_field('precio_10'); ?></strong><em>MXN</em></p>
												<p class="kilometros"><strong>SERVICIO DE 10, 30, 50, 70 & 90</strong> MIL KM</p>
											<?php endif; ?>
											<?php if( get_field('lista_de_servicios_10') ): ?>
												<?php the_field('lista_de_servicios_10'); ?>
											<?php endif; ?>
										</div>
										<div id="km20" class="tab">
											<?php if( get_field('precio_20') ): ?>
												<p class="price"><strong>$ <?php the_field('precio_20'); ?></strong><em>MXN</em></p>
												<p class="kilometros"><strong>SERVICIO DE 20 & 60</strong> MIL KM</p>
											<?php endif; ?>
											<?php if( get_field('lista_de_servicios_20') ): ?>
												<?php the_field('lista_de_servicios_20'); ?>
											<?php endif; ?>
										</div>
										<div id="km40" class="tab">
											<?php if( get_field('precio_40') ): ?>
												<p class="price"><strong>$ <?php the_field('precio_40'); ?></strong><em>MXN</em></p>
												<p class="kilometros"><strong>SERVICIO DE 40 & 80</strong> MIL KM</p>
											<?php endif; ?>
											<?php if( get_field('lista_de_servicios_40') ): ?>
												<?php the_field('lista_de_servicios_40'); ?>
											<?php endif; ?>
										</div>
										<div id="km100" class="tab">
											<?php if( get_field('precio_100') ): ?>
												<p class="price"><strong>$ <?php the_field('precio_100'); ?></strong><em>MXN</em></p>
												<p class="kilometros"><strong>SERVICIO DE 100</strong> MIL KM</p>
											<?php endif; ?>
											<?php if( get_field('lista_de_servicio_100') ): ?>
												<?php the_field('lista_de_servicio_100'); ?>
											<?php endif; ?>
										</div>
									</div>
									<?php if( get_field('imagen_de_seccion_de_precio_de_servicios') ): ?>
									<figure class="imagefill">
										<?php
											$image = get_field('imagen_de_seccion_de_precio_de_servicios');
											echo wp_get_attachment_image($image, 'image');
										?>
									</figure>
									<?php endif; ?>
									
								</div>

						        <div class="form-cotizacion-modelo cf">
						        	<?php if ( has_post_thumbnail() ) : ?>
						        	
										<figure class="imagefill">
											<?php the_post_thumbnail("large"); ?>
										</figure>
										
									<?php endif;?>
									<div class="col-md-6">

										<h3 class="text_intro">¡QUIERO ESTE <?php echo the_title(); ?>!</h3>

										<?php echo do_shortcode('[contact-form-7 id="237" title="Modelos"]' ); ?>
									</div>
						        </div>
						</div><!-- .entry-content -->
						<?php endif; ?>

						<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
					</article><!-- #post-## -->

				<?php endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.waypoints.min.js"></script>
	<script type="text/javascript">

		jQuery(document).ready(function(){
			jQuery('.btn-reel:first-child').addClass("active");
			jQuery('.btn-reel').click(function(e){
				var separador = ",";
				jQuery(".btn-reel").removeClass("active");
				jQuery(this).addClass("active");
				var dataImage = jQuery(this).attr('data-images').split(separador);
				jQuery("#image").reel('images', dataImage);
				e.preventDefault();
		    });

		    var waypoint = new Waypoint({
				element: jQuery('.content-modelos'),
				handler: function(direction) {
					jQuery('#car_detail').addClass('slideInRight');
					
					setTimeout(function(){
						jQuery('#num_caballaje').addClass('fadeInDown');
						/*jQuery({someValue: 10}).animate({someValue: jQuery('#num_caballaje strong').text()}, {
							duration: 2000,
							easing:'swing',
							step: function() { 
								jQuery('#num_caballaje strong').text(parseFloat(this.someValue.toFixed(0)));
							}
						});*/
					}, 1000);
					setTimeout(function(){
						jQuery('#num_rendimiento').addClass('fadeInDown');
						/*jQuery({someValue: 10}).animate({someValue: jQuery('#num_rendimiento strong').text()}, {
							duration: 2000,
							easing:'swing',
							step: function() { 
								jQuery('#num_rendimiento strong').text(parseFloat(this.someValue.toFixed(1)));
							}
						});*/
					}, 1500);
				},
				offset: '-50%'
			})
		});
	</script>

<?php
get_footer();
