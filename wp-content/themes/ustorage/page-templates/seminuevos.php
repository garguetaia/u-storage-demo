<?php
/**
 * The Template for displaying all single posts
 * Template Name: Seminuevos
 * @package WordPress
 * @subpackage Mazda_Ravisa
 * @since Mazda Ravisa 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content seminuevos" role="main">
			
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post(); 

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<header class="entry-header">
							<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && ustorage_categorized_blog() ) : ?>
							<div class="entry-meta">
								<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'mazdaravisa' ) ); ?></span>
							</div>
							<?php
								endif;

								if ( is_single() ) :
									the_title( '<h1 class="entry-title">', '</h1>' );
								else :
									the_title( '<h1 class="entry-title">', '</h1>' );
								endif;
							?>

							<div class="entry-meta">
								<?php
									if ( 'post' == get_post_type() )
										mazdaravisa_posted_on();

									if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
								?>
								<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'mazdaravisa' ), __( '1 Comment', 'mazdaravisa' ), __( '% Comments', 'mazdaravisa' ) ); ?></span>
								<?php
									endif;

									edit_post_link( __( 'Edit', 'mazdaravisa' ), '<span class="edit-link">', '</span>' );
								?>
							</div><!-- .entry-meta -->
						</header><!-- .entry-header -->

						<?php if ( is_search() ) : ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php else : ?>

						<ul class="list_seminuevos cf">
							<?php
								
								query_posts( array ( 'category_name' => 'seminuevos', 'posts_per_page' => -1, 'post_type' => 'product', 'order' => 'ASC' ) );
								if ( have_posts() ) :
									// Start the Loop.
									while ( have_posts() ) : the_post();?>
										<li>
											<a href="#semi-<?php the_ID();?>" title="<?php the_title();?>" class="cf grid__item">
												<div class="loader"></div>
												<div class="content_info">
													<?php
														the_title( '<h2 class="title-seminuevo">', '</h2>' );
													?>
													<?php if( get_field('precio') ): ?>
														<p class="price_auto">$ <?php echo number_format( floatval( get_field('precio') ), 0, '.', ',' ); ?></p>
													<?php endif; ?>
													<?php if( get_field('kilometros') ): ?>
														<p class="km_auto"><?php echo number_format( floatval( get_field('kilometros') ), 0, '.', ',' ); ?> KM</p>
													<?php endif; ?>
													<hr>
													<p><?php the_field('modelo'); ?> <span><?php the_field('year'); ?></span> <?php the_field('garantia'); ?></p>
												</div>
												<?php if ( has_post_thumbnail() ) : ?>
													<figure class="imagefill">
														<?php the_post_thumbnail("medium"); ?>
														<figcaption>Ver Galería</figcaption>
													</figure>
												<?php endif; ?>
											</a>
										</li>

									<?php endwhile;
									// Previous/next post navigation.
									mazdaravisa_paging_nav();

								else :
									// If no content, include the "No posts found" template.
									

								endif;

							?>
						</ul>
						<section class="content_seminuevos">
							<div class="scroll-wrap">
						<?php
							if ( have_posts() ) :
								// Start the Loop.

								while ( have_posts() ) : the_post();?>
								
									<article id="semi-<?php the_ID();?>" class="box_gallery content__item">
										
										<div class="list_seminuevos">
											<div class="content_info">
												<?php
													the_title( '<h2 class="title-seminuevo">', '</h2>' );
												?>
												<p class="price_auto"><?php echo get_post_meta($post->ID, 'precio', true); ?></p>
												<p class="km_auto"><?php echo get_post_meta($post->ID, 'kilometros', true);?></p>
												<hr>
												<p><?php echo get_post_meta($post->ID, 'marca', true); ?> <span><?php echo get_post_meta($post->ID, 'year', true); ?></span> <?php echo get_post_meta($post->ID, 'garantia', true); ?></p>
											</div>
											<?php if ( has_post_thumbnail() ) : ?>
												<figure class="imagefill">
													<?php the_post_thumbnail("large"); ?>
												</figure>
											<?php endif; ?>
										</div> 
								        <?php the_content (); ?>

								        <button class="btn-addAuto" type="submit">Quiero este auto</button>

								        <div class="form-cotizacion cf">
								        	<?php if ( has_post_thumbnail() ) : ?>
								        	
												<figure class="imagefill">
													<?php the_post_thumbnail("large"); ?>
												</figure>
												
											<?php endif;?>

											<?php echo do_shortcode('[contact-form-7 id="154" title="Cotización"]' ); ?>
								        </div>

								     </article>
								<?php endwhile;
							else :
								// If no content, include the "No posts found" template.
								echo 'Por el momento no hay ningún auto seminuevo';

							endif;
						?>
							</div>
							<button type="button" class="btnClose"></button>
						</section>
						<div class="entry-content">
							<?php
								wp_reset_query();
								/* translators: %s: Name of current post */
								the_content( sprintf(
									__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'mazdaravisa' ),
									the_title( '<span class="screen-reader-text">', '</span>', false )
								) );

								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'mazdaravisa' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
								) );
							?>
						</div><!-- .entry-content -->
						<?php endif; ?>

						<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
					</article><!-- #post-## -->

				<?php endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
	

	<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main_seminuevos.js"></script>
<?php
get_footer();
