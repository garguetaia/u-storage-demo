<?php
/**
 * Template Name: Servicios
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && ustorage_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


	<div id="content" class="site-content servicios" role="main">
		<?php
			// Start the Loop.
			while ( have_posts() ) : the_post(); ?>

				
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry__header">
					<?php
						// Page thumbnail and title.
						ustorage_page_thumbnail();
						the_title( '<h1 class="entry-title">', '</h1>' );

					?>
					<?php if( get_field('subtitle_page') ): ?>
						<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
					<?php endif; ?>
				</header><!-- .entry-header -->
				<?php the_content(); ?>
			</article><!-- #post-## -->


				<?php // If comments are open or we have at least one comment, load up the comment template.
			endwhile;
		?>

		<section class="site__container">
			<ul class="list_service cf">
				<?php
					
					query_posts( array ('posts_per_page' => -1, 'post_type' => 'servicios', 'order' => 'ASC' ) );
					if ( have_posts() ) :
						// Start the Loop.
						while ( have_posts() ) : the_post();?>
							<li class="list_service_content">
								<?php if ( has_post_thumbnail() ) : ?>
									<figure class="list_service_content__foto">
										<?php the_post_thumbnail("large"); ?>
									</figure>
								<?php endif; ?>
								<div class="group_text">
									<h4 class="subtitle naranja"><?php the_title(); ?></h4>
									<div class="txts"><?php the_content(); ?></div>
								</div>
							</li>

						<?php endwhile;
						// Previous/next post navigation.
						ustorage_paging_nav();

					else :
						// If no content, include the "No posts found" template.
						

					endif;
					wp_reset_query();
				?>
			</ul>
			
		</section><!-- .entry-content -->

		<section class="home__benefits">
			<h4 class="subtitle morado">BENEFICIOS</h4>
			<ul class="benefits">
				<li class="benefits--icon">
					<a href="<?php bloginfo('url'); ?>/quienes-somos/">
						<figure class="benefits--icon__security">
							<img src="<?php bloginfo('template_url'); ?>/images/ico-seguridad.svg" alt="Seguridad">
						</figure>
						<span>SEGURIDAD</span>
					</a>
				</li>
				<li class="benefits--icon">
					<a href="<?php bloginfo('url'); ?>/quienes-somos/">
						<figure class="benefits--icon__confort">
							<img src="<?php bloginfo('template_url'); ?>/images/ico-comodidad.svg" alt="Comodidad">
						</figure>
						<span>COMODIDAD</span>
					</a>
				</li>
				<li class="benefits--icon">
					<a href="<?php bloginfo('url'); ?>/quienes-somos/">
						<figure class="benefits--icon__clean">
							<img src="<?php bloginfo('template_url'); ?>/images/ico-limpieza.svg" alt="Limpieza">
						</figure>
						<span>LIMPIEZA</span>
					</a>
				</li>
				<li class="benefits--icon">
					<a href="<?php bloginfo('url'); ?>/quienes-somos/">
						<figure class="benefits--icon__access">
							<img src="<?php bloginfo('template_url'); ?>/images/ico-accesabilidad.svg" alt="Accsesibilidad">
						</figure>
						<span>ACCESIBILDIAD</span>
					</a>
				</li>
			</ul>
		</section>
	</div><!-- #content -->

</div><!-- #main-content -->
<?php

get_footer();
