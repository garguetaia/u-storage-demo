<?php
/**
 * Template Name: Sucursal
 *
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && ustorage_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content location" role="main">

			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();?>

					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry__header">
							<?php
								// Page thumbnail and title.
								ustorage_page_thumbnail();
								the_title( '<h1 class="entry-title">', '</h1>' );

							?>
							<?php if( get_field('subtitle_page') ): ?>
								<h2 class="entry-subtitle"><?php the_field('subtitle_page'); ?></h2>
							<?php endif; ?>
						</header><!-- .entry-header -->

						<div class="entry-content">
							<?php
								the_content();
								wp_link_pages( array(
									'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
									'after'       => '</div>',
									'link_before' => '<span>',
									'link_after'  => '</span>',
								) );

								edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
							?>
						</div><!-- .entry-content -->
					</article><!-- #post-## -->
					
				<?php endwhile;
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_footer();
