<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<figure class="imagefill">
		<?php
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
			
		?>
	</figure>

	<header class="entry-header">
		<?php

			if ( is_single() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title">', '</h2>' );
			endif;
		?>

		<div class="entry-meta">
			<?php
				if( get_field('start_date') ):
					$dateformatstring = "d F";
					$unixtimestamp = strtotime(get_field('start_date'));

					echo "<span class='entry-date'> Válido del ". date_i18n($dateformatstring, $unixtimestamp) . "</span>";
				endif;

				if( get_field('end_date') ):
					$dateformatstring = "d F, Y";
					$unixtimestamp = strtotime(get_field('end_date'));

					echo "<span class='entry-date'> al ". date_i18n($dateformatstring, $unixtimestamp) . "</span>";
				endif;
			?>
			
			<?php
				

				edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
			?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<?php if ( is_search() ) : ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	<?php else : ?>
	<div class="entry-content">
		<?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'ustorage' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
		?>
	</div><!-- .entry-content -->
	<?php endif; ?>

	<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</article><!-- #post-## -->