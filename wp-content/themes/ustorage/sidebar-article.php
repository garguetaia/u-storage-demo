<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */
?>
<div id="secondary">
	<?php
	// get all the categories from the database
	$cats = get_categories();
	$firstPosts = get_the_ID();

	// loop through the categries
	foreach ($cats as $cat) {
		// setup the cateogory ID
		$cat_id= $cat->term_id;
		// Make a header for the cateogry
		echo "<h2>".$cat->name."</h2>";
		// create a custom wordpress query
		query_posts( array(
			'post__not_in' => $firstPosts,
			'cat'  => $cat_id,
			'posts_per_page' => 1,
			'orderby' => 'rand',
		) );
		// start the wordpress loop!
		if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php // create our link now that the post is setup ?>
			<a href="<?php the_permalink();?>"><?php the_title(); ?></a>
			<?php echo '<hr/>'; ?>

		<?php endwhile; endif; // done our wordpress loop. Will start again for each category ?>
	<?php } // done the foreach statement 
	wp_reset_query();?>
</div><!-- #secondary -->
