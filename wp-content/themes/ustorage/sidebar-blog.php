<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

?>
<section class="site__container">
	<?php

	$element = 1;

	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'orderby' => 'date',
		'posts_per_page' => 9,
	);

	$query = new WP_Query( $args );

	$tax = 'post_tag';
	$terms = get_categories( array(
		'orderby' => 'name',
		'parent'  => 0
	) );
	$count = count( $terms );

	global $post;

	if ( $count > 0 ): ?>
		<nav class="post-tags">
			<h3>CATEGORÍAS</h3>
			<ul>
				<li><a href="#" class="tax-filter" title="">LO MÁS NUEVO</a></li>
			<?php
			foreach ( $terms as $term ) {
		 		$term_link = get_term_link( $term, $tax );
				echo '<li class="' . $term->slug . '"><a href="' . $term_link . '" class="tax-filter" title="' . $term->slug . '">' . $term->name . '</a></li> ';
		 	} ?>
	 		</ul>
		</nav>
	<?php endif;
	if ( $query->have_posts() ): ?>
		<div class="tagged-posts">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<?php while ( $query->have_posts() ) : $query->the_post();

				if($element > 6):
					$element = 1;
				endif;

				if (! is_single() ) :
					$classes = array(
						'grid-item',
						'grid-item--width'.$element,
					);
				endif;

				?>

				<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
					<figure class="imagefill">
						<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
						<?php
							if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'page-templates/full-width.php' ) ) ) {
								the_post_thumbnail( 'ustorage-full-width' );
							} else {
								the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
							}
						?>
						</a>
					</figure>

					<header class="entry-header">
						<?php

							if ( is_single() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							endif;
						?>

						<div class="entry-meta">
							<?php
								if ( 'post' == get_post_type() )
									ustorage_posted_on();

								if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
							?>
							<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'ustorage' ), __( '1 Comment', 'ustorage' ), __( '% Comments', 'ustorage' ) ); ?></span>
							<?php
								endif;

								edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
							?>
						</div><!-- .entry-meta -->
					</header><!-- .entry-header -->

					<?php if ( !is_single() ) : ?>
					<div class="entry-summary">
						<?php echo "<p>".excerpt(30)."</p>"; ?>
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content">
						<?php
							/* translators: %s: Name of current post */
							the_content( sprintf(
								__( '<span class="meta-nav">...</span>', 'ustorage' ),
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );

							wp_link_pages( array(
								'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
								'after'       => '</div>',
								'link_before' => '<span>',
								'link_after'  => '</span>',
							) );
						?>
					</div><!-- .entry-content -->
					<?php endif; ?>

					<?php 
						if ( !is_single() ) :
							echo '<div class="btn-link"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">LEER</a></div>';
						endif;
					?>
					<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
				</article><!-- #post-## -->
				<?php

				$element = $element + 1;

			endwhile; ?>
		</div>

	<?php else :
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );
		endif; ?>
</section>
