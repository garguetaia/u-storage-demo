<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

?>
<section class="site__container">
	<?php

	$args = array(
		'post_type' => 'faqs',
		'orderby' => 'date',
	);

	$query = new WP_Query( $args );

	$tax = 'post_tag';

	global $post;?>

	<div class="tips_description">
		<h3>¿NO ENCUENTRAS LA INFORMACIÓN QUE NECESITAS?</h3>
		<p>LLÁMANOS 01 800 966 0000</p>
	</div>

	
	<?php if ( $query->have_posts() ): ?>

		<div class="post-faqs">
			<?php while ( $query->have_posts() ) : $query->the_post();?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					

					
					<?php

						if ( is_single() ) :
							the_title( '<h1 class="entry-title">', '</h1>' );
						else :
							the_title( '<h2 class="entry-title"><a href="#0" class="cd-faq-trigger" rel="bookmark">', '</a></h2>' );
						endif;
					?>

					<?php if ( is_search() ) : ?>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content cd-faq-content">
						<?php
							/* translators: %s: Name of current post */
							the_content( sprintf(
								__( '<span class="meta-nav">...</span>', 'ustorage' ),
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );

							wp_link_pages( array(
								'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
								'after'       => '</div>',
								'link_before' => '<span>',
								'link_after'  => '</span>',
							) );
						?>
					</div><!-- .entry-content -->
					<?php endif; ?>
					

					
				</article><!-- #post-## -->
				<?php

			endwhile; ?>
		</div>

	<?php else :
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );
		endif; ?>
</section>
