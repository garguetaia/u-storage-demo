<?php
/**
 * The Content Sidebar
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

?>
<section class="site__container">
	<?php

	$element = 1;

	$args = array(
		'post_type' => 'tip',
		'orderby' => 'date',
		'posts_per_page' => 6,
	);

	$query = new WP_Query( $args );

	$tax = 'post_tag';

	global $post;?>

	<div class="tips_description">
		<h3>TE OFRECEMOS ESTAS RECOMENDACIONES</h3>
		<p>Para almacenar tus artículos de la mejor manera:</p>
	</div>

	
	<?php if ( $query->have_posts() ): ?>

		<div class="post-tips">
			<?php while ( $query->have_posts() ) : $query->the_post();?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					

					<header class="entry-header">
						<?php

							if ( is_single() ) :
								the_title( '<h1 class="entry-title">', '</h1>' );
							else :
								the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
							endif;
						?>

					</header><!-- .entry-header -->

					<?php if ( is_search() ) : ?>
					<div class="entry-summary">
						<?php the_excerpt(); ?>
					</div><!-- .entry-summary -->
					<?php else : ?>
					<div class="entry-content">
						<?php
							/* translators: %s: Name of current post */
							the_content( sprintf(
								__( '<span class="meta-nav">...</span>', 'ustorage' ),
								the_title( '<span class="screen-reader-text">', '</span>', false )
							) );

							wp_link_pages( array(
								'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
								'after'       => '</div>',
								'link_before' => '<span>',
								'link_after'  => '</span>',
							) );
						?>
					</div><!-- .entry-content -->
					<?php endif; ?>
					<figure class="imagefill">
						<?php if( get_field('url_youtube_tips') ): ?>
						<a class="viewvideo feature-modal-btn" data-open="videoModal" data-video="<?php the_field('url_youtube_tips'); ?>">
						<?php
							if ( ( ! is_active_sidebar( 'sidebar-2' ) || is_page_template( 'page-templates/full-width.php' ) ) ) {
								the_post_thumbnail( 'ustorage-full-width' );
							} else {
								the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
							}
						?>
						</a>
						<?php endif; ?>
					</figure>

					<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
				</article><!-- #post-## -->
				<?php

				$element = $element + 1;

			endwhile; ?>
		</div>

	<?php else :
			// If no content, include the "No posts found" template.
			get_template_part( 'content', 'none' );
		endif; ?>
</section>
