<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage U_Storage
 * @since U-Storage 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				$firstPosts = array();
				while ( have_posts() ) : the_post();

					$firstPosts[] = $post->ID;
					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					/* get_template_part( 'content', get_post_format() );*/
					?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php ustorage_page_thumbnail(); ?>

						<header class="entry-header">
							<?php

								if ( is_single() ) :
									the_title( '<h1 class="entry-title">', '</h1>' );
								else :
									the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
								endif;
							?>
						</header><!-- .entry-header -->

						<?php if ( is_search() ) : ?>
						<div class="entry-summary">
							<?php the_excerpt(); ?>
						</div><!-- .entry-summary -->
						<?php else : ?>

						<section class="site__container clearfix">
							<div class="entry-content">
								<?php if (is_single() ) : 
									the_title( '<h1 class="entry-title">', '</h1>' );?>
									<div class="entry-meta">
										<?php
											if ( 'post' == get_post_type() )
												printf( '<span class="entry-date">Publicado el: <time class="entry-date" datetime="%2$s">%3$s</time></span> <span class="byline">por: <span class="author vcard">%5$s</span> Categoría:</span>',
													esc_url( get_permalink() ),
													esc_attr( get_the_date( 'c' ) ),
													esc_html( get_the_date() ),
													esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
													get_the_author()
												);
												$categories = get_the_terms( get_the_ID(), 'category' );
												foreach( $categories as $category ) {
												    echo '<a href="'.get_bloginfo('url').'/blog/?category='.$category->slug.'"><span class="post-categories '.$category->slug.'">'. $category->name . '</span></a>';
												}

											if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
										?>
										<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'ustorage' ), __( '1 Comment', 'ustorage' ), __( '% Comments', 'ustorage' ) ); ?></span>
										<?php
											endif;

											edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
										?>
									</div><!-- .entry-meta -->
								<?php endif; ?>

								<?php
									/* translators: %s: Name of current post */
									the_content( sprintf(
										__( '<span class="meta-nav">...</span>', 'ustorage' ),
										the_title( '<span class="screen-reader-text">', '</span>', false )
									) );

									wp_link_pages( array(
										'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'ustorage' ) . '</span>',
										'after'       => '</div>',
										'link_before' => '<span>',
										'link_after'  => '</span>',
									) );
								?>
							</div><!-- .entry-content -->

							<section class="sidebar">
								<h2>Otros artículos</h2>
								<?php
								// get all the categories from the database
								$cats = get_categories(); 

								// loop through the categries
								foreach ($cats as $cat) {
									// setup the cateogory ID
									$cat_id= $cat->term_id;
									
									// create a custom wordpress query
									query_posts( array(
										'post__not_in' => $firstPosts,
										'cat'  => $cat_id,
										'posts_per_page' => 1,
										'orderby' => 'rand',
									) );
									// start the wordpress loop! ?>
									<div class="group-category">
									<?php if (have_posts()) : while (have_posts()) : the_post();
										echo "<h3 class='".$cat->slug."'>".$cat->name."</h2>";
									?>
										<article>
											<figure>
												<?php the_post_thumbnail( 'thumbnail', array( 'alt' => get_the_title() ) ); ?>
											</figure>
											<?php // create our link now that the post is setup ?>
											<h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
											<div class="entry-meta">
												<?php
													if ( 'post' == get_post_type() )
														printf( '<span class="entry-date"><time class="entry-date" datetime="%2$s">%3$s</time></span>',
															esc_url( get_permalink() ),
															esc_attr( get_the_date( 'c' ) ),
															esc_html( get_the_date() ),
															esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
															get_the_author()
														);
												?>
												
												<?php
													

													edit_post_link( __( 'Edit', 'ustorage' ), '<span class="edit-link">', '</span>' );
												?>
											</div><!-- .entry-meta -->
											<a href="<?php esc_url( get_permalink() ) ?>" class="btn-more" rel="bookmark">LEER <i class="genericon genericon-next"></i></a>
										</article>

									<?php endwhile; endif; // done our wordpress loop. Will start again for each category ?>
									</div>
								<?php } // done the foreach statement
								wp_reset_query();
								?>
							</section>
						</section>
						
						<?php endif; ?>

						<?php 
							if ( !is_single() ) :
								echo '<a href="' . esc_url( get_permalink() ) . '" class="btn-more" rel="bookmark">LEER <i class="genericon genericon-next"></i></a>';
							endif;
						?>
						<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
					</article><!-- #post-## -->
					<?php // Previous/next post navigation.
					ustorage_post_nav();

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
				endwhile;
				
			?>
			
			<div class="clearfix"></div>
		</div><!-- #content -->
	</div><!-- #primary -->


<?php
get_footer();
